/**
*Ejercicio 1
*
*Generar aleatoriamente un arreglo lineal a de 120
*elementos numéricos, con enteros entre 1000 y 1500
*y mostrarlos. Luego ingresar dos valores en las
*variables m y p. El valor m debe asignar a la
*posición p del arreglo. Mostrar el vector modificado.
*
*/

#include <iostream>
#include <cstdlib>
#include <time.h>

using namespace std;

int main(int argc, char *argv[]) {
    srand(time(0));
    int arreglo[120];
    int m, p;

    for (int i=0;i<120;i++){
        arreglo[i]=1000+rand()%500; //Asigna valores entre 1000 y 1500 al vector arreglo;
    }

    for (int i=0;i<120;i++){
        cout<<arreglo[i]<<ends; //Muestra el vector arreglo;
    }

    cout<<endl<<"Ingrese la posicion y el valor: "<<endl;
    cin>>p>>m;
    p=p-1;  //p es la posicion que tendria en el vector;
    cout<<endl;

    arreglo[p]=m;   //El valor m se asigna a la posicion p del vetor arreglo;

    for (int i=0;i<120;i++){
        cout<<arreglo[i]<<ends; //Muestra el vector arreglo modificado;
    }
    cout<<endl;

    system("pause");
    return 0;
}
