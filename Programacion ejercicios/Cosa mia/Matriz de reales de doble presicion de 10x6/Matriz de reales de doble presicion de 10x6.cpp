/**
*Ejercicio 8
*
*Usando notaci�n de punteros genere aleatoriamente una matriz de n�meros reales de
*doble precisi�n de 10 filas por 6 columnas y determine e informe:
*
*a) El promedio de la fila que el usuario ingrese como dato.
*
*b) La suma de cada columna.
*
*/

#include <iostream>
#include <cstdlib>
#include <time.h>
#include <iomanip>

using namespace std;

int main(int argc, char *argv[]) {
    srand(time(0));
    float c[6]={0};
    int fila;
    float prom;
    int acum=0;
    float **matriz;
    matriz=new float* [10];
    for (int i=0;i<10;i++){
        matriz[i]=new float [6];
    }

    cout<<fixed<<setprecision(2);
    for (int i=0;i<10;i++){
        for (int e=0;e<6;e++){
            matriz[i][e]=1+(rand()%150)/3.00;
        }
    }

    for (int i=0;i<10;i++){
        for (int e=0;e<6;e++){
            cout<<matriz[i][e]<<ends<<ends;
        }
        cout<<endl;
    }

    ///PUNTO A:
    cout<<"Ingrese el numero de la fila que desea saber el promedio:"<<ends;
    cin>>fila;
    for (int i=0;i<6;i++){
        acum+=matriz[fila-1][i];
    }
    prom=acum/6.00;
    cout<<"Promedio de la fila "<<fila<<": "<<prom<<endl;

    ///PUNTO B:
    for (int i=0;i<10;i++){
        for (int e=0;e<6;e++){
            c[e]+=matriz[i][e];
        }
    }
    for (int i=0;i<6;i++){
        cout<<"Suma de la columna "<<i+1<<": "<<c[i]<<endl;
    }

    for (int i=0;i<10;i++){
        delete[] matriz[i];
    }
    delete[] matriz;
    system("pause");
    return 0;
}
