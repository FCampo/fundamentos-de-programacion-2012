/**
*
* Ejercicio 16
*
* Considere un struct frac que contiene dos miembros enteros a y b que representan el
* numerador y el denominador de una fracci�n. Genere una funci�n que reciba dos
* par�metros de tipo frac y devuelva la suma de los dos valores fraccionarios. Proponga un
* programa C++ que utilice esta funci�n.
*
**/

#include <iostream>
#include <cstdlib>
#include <iomanip>

using namespace std;

typedef struct {
    int a;
    int b;
}Frac;

Frac Suma_Frac(Frac fr, Frac fr2);

int main(int argc, char *argv[]) {

    Frac fraccion, fraccion2;

    cout<<"fraccion 1: "<<endl;
    cout<<"numerador: "<<ends;
    cin>>fraccion.a;
    cout<<"denominador: "<<ends;
    cin>>fraccion.b;
    cout<<"fraccion 2: "<<endl;
    cout<<"numerador: "<<ends;
    cin>>fraccion2.a;
    cout<<"denominador: "<<ends;
    cin>>fraccion2.b;

    Frac sum=Suma_Frac(fraccion, fraccion2);
    cout<<"Suma de las dos fracciones: "<<sum.a<<"/"<<sum.b<<endl;

    system("pause");
    return 0;
}

Frac Suma_Frac(Frac fr, Frac fr2){

    Frac suma;

    suma.a=((fr.a*fr2.b)+(fr.b*fr2.a));
    suma.b=(fr.b*fr2.b);

    for(int i=2;i<suma.b;i++){
        if (((suma.a%i)==0)&&((suma.b%i)==0)){
            suma.a=suma.a/i;
            suma.b=suma.b/i;
            break;
        }
    }

    return suma;
}
