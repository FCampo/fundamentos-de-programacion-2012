/**
*Ejercicio 12
*
*Escriba la funci�n Plural() cuyo prototipo se expone a continuaci�n del enunciado. Debe
*retornar en el mismo par�metro el plural correspondiente. Considere agregar 's' si la
*palabra termina con vocal y 'es' si termina en consonante.
*
*void plural (char *p);
*
*/

#include <iostream>
#include <cstdlib>
#include <cstring>
#include <stdio.h>

using namespace std;

void plural (char *p);

int main(int argc, char *argv[]) {

    char plur[50];

    gets(plur);
    plural(plur); //Llamada a la funcion plural creada en el programa;
    puts(plur);

    system("pause");
    return 0;
}

void plural (char *p){

    int tam=strlen(p);
    int opc=0;

    if(p[tam-1]>='A'){  //Determina si la palabra esta en mayuscula;
        if(p[tam-1]<='Z'){
            opc=1;
        }
    }

    //se usa la funcion tolower para poder comparar en el caso de que la palabra este en mayuscula;
    if((tolower(p[tam-1])=='a')||(tolower(p[tam-1])=='e')||(tolower(p[tam-1])=='i')||(tolower(p[tam-1])=='o')||(tolower(p[tam-1])=='u')){
        p[tam]='s';     //Si la palabra termina en una vocal se agrega una "s" al final para convertirla en plural;
        p[tam+1]='\0';
    }
    else if((tolower(p[tam-1])>'a')&&(tolower(p[tam-1])<'z')&&(tolower(p[tam-1])!='s')){
            p[tam]='e';
            p[tam+1]='s';   //Si no, se agrega "es" para convertirla en plural;
            p[tam+2]='\0';
        }
        else if(tolower(p[tam-1])=='z'){
                p[tam-1]='c';
                p[tam]='e';
                p[tam+1]='s'; //Si la palabra termina en "z" se cambia la "z" por "ces" al final para convertirla en plural;
                p[tam+2]='\0';
            }
            else if(tolower(p[tam-1])=='s'){
                p[tam-1]='n';   //Si la palabra termina en "s" se cambia la "s" por una "n" para convertirla en plural;
            }

    if(opc==1){ //Si la palabra esta en mayuscula la devuelve en mayuscula, si no, la devuelve en minuscula;
        strupr(p);
    }
    else strlwr(p);
}
