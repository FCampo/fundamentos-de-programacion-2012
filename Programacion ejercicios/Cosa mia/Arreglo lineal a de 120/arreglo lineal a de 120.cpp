/**
*
*Ejercicio 6
*
*Utilizando notaci�n de punteros generar aleatoriamente un arreglo lineal A de 120
*elementos num�ricos, con enteros entre 1000 y 1500 y mostrarlo en pantalla. Utilice
*memoria din�mica.
*
*Ejercicio 7
*
*Ampl�e el programa anterior para que luego de generar el arreglo aleatorio, permita
*ingresar un valor M que debe ser insertado en la posici�n 32 de dicho arreglo. Informar el
*vector modificado. Nota: al ingresar un elemento en la posici�n 32, el arreglo original
*aumenta en su tama�o.
*
*/

#include <iostream>
#include <cstdlib>
#include <time.h>

using namespace std;

int main(int argc, char *argv[]) {
    srand(time(0));
    int *A;
    A=new int [120];
    int M;
    int aux[120];
    int e=0;

    for (int i=0;i<120;i++){
        A[i]=1000+(rand()%500); //Llena el vector dinamico A con valores aleatorios entre 1000 y 1500;
    }

    for(int i=0;i<120;i++){
        cout<<i+1<<") "<<A[i]<<endl;    //Muestra por pantalla el vector A;
    }
    cout<<endl;

    cout<<"Ingresar el valor a insertar en la linea 32: "<<ends;
    cin>>M;

    for (int i=0;i<120;i++){    //Copia el Vector A en el vector auxiliar;
        aux[i]=A[i];
    }
    delete[] A; //Libera la memoria apuntada;
    A=new int [121];    //Apunta a un nuevo bloque de memoria;

    for (int i=0;i<121;i++){ //Cuando se llegue a la pocicion 32 se insertara el nuevo valor y se seguira copiando el vector auxiliar en el vector A;
        if(i==31){
            A[i]=M;
            continue;
        }
        A[i]=aux[e];
        e++;
    }

    for(int i=0;i<121;i++){ //Muestra por pantalla el vector modificado;
        cout<<i+1<<") "<<A[i]<<endl;
    }
    cout<<endl;

    delete[] A; //Libera la memoria apuntada;
    system("pause");
    return 0;
}
