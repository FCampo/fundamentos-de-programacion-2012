/**
*
* Ejercicio 5
*
* En un curso de matem�tica que tiene 5 comisiones con no m�s de 25 alumnos (pueden ser menos) se han tomado 2 parciales
* y 1 recuperatorio a lo largo del cursado.
*
* Escriba un programa en C++ que lea por pantalla los siguientes datos de cada alumno:
*   *N�mero de matr�cula
*   *N�mero de comisi�n a la que pertenece (1 � 5)
*   *Las notas obtenidas en los dos parciales y el recuperatorio (si no se present� a rendir, le corresponde un 0).
* La carga de datos finaliza con el valor de matr�cula 0.
* Almacene la informaci�n que crea necesaria utilizando arreglos.
* A partir de los arreglos el programa debe obtener e informar:
*   *i) La cantidad de alumnos por comisi�n que rindieron el recuperatorio.
*   *ii) El porcentaje de alumnos regulares y el porcentaje de alumnos promovidos para todo el curso.
*
* Tenga en cuenta que la condici�n para regularizar la materia es haber obtenido 60 puntos o m�s en cada uno de los parciales,
* pudiendo recuperar s�lo uno de ellos.
* La promoci�n se obtiene habiendo conseguido 80 puntos o m�s en cada uno de los parciales,
* no hay opci�n de recuperar para promocionar.
*
**/

#include <iostream>
#include <cstdlib>

using namespace std;

struct Alumnos{
    int nro_matricula, nro_comision, parcial[2], recuperatorio;
};

int main(int argc, char *argv[]) {

    Alumnos alumno[125];
    char opc;
    int i=0, cant_recc[5]={0};
    int cant_prom=0, cant_reg=0;

    cout<<"Ingrese matricula del alumno: (0 finalizar ingreso)"<<ends;
    cin>>alumno[i].nro_matricula;
    while (alumno[i].nro_matricula!=0){
        cout<<"Ingrese comision del alumno:"<<ends;
        cin>>alumno[i].nro_comision;
        cout<<"Ingrese nota del primer parcial:"<<ends;
        cin>>alumno[i].parcial[0];
        cout<<"Ingrese nota del segundo parcial:"<<ends;
        cin>>alumno[i].parcial[1];
        cout<<"Rindio recuperatorio? (s/n)"<<ends;
        cin>>opc;
        if(tolower(opc)=='n'){
            alumno[i].recuperatorio=0;
        }
        else{
            cout<<"Ingrese nota del recuperatorio:"<<ends;
            cin>>alumno[i].recuperatorio;
        }
        i++;
        cout<<"Ingrese matricula del alumno: (0 finalizar ingreso)"<<ends;
        cin>>alumno[i].nro_matricula;
    }

    for (int e=0;e<i;e++){
        switch (alumno[e].nro_comision){
        case 1: if(alumno[e].recuperatorio!=0){
                    cant_recc[0]++;
                }
            break;
        case 2: if(alumno[e].recuperatorio!=0){
                    cant_recc[1]++;
                }
            break;
        case 3: if(alumno[e].recuperatorio!=0){
                    cant_recc[2]++;
                }
            break;
        case 4: if(alumno[e].recuperatorio!=0){
                    cant_recc[3]++;
                }
            break;
        case 5: if(alumno[e].recuperatorio!=0){
                    cant_recc[4]++;
                }
            break;
        }
    }

    for (int e=0;e<i;e++){
        if (alumno[e].recuperatorio>=60){
            if((alumno[e].parcial[0]<60)&&(alumno[e].parcial[1]>=60)){
                cant_reg++;
            }
            if((alumno[e].parcial[1]<60)&&(alumno[e].parcial[0]>=60)){
                cant_reg++;
            }
        }
        else if(alumno[e].recuperatorio==0){
            if((alumno[e].parcial[0]>=80)&&(alumno[e].parcial[1]>=80)){
               cant_prom++;
            }
            else if((alumno[e].parcial[0]>=60)&&(alumno[e].parcial[1]>=60)){
                cant_reg++;
            }
        }
    }

    for (int e=0;e<5;e++){
        cout<<"Cantidad de alumnos que recuperaron (com "<<e+1<<"): "<<cant_recc[e]<<endl;
    }
    cout<<"Cantidad de alumnos regurales en el curso: "<<cant_reg<<endl;
    cout<<"Cantidad de alumnos promocionales en el curso: "<<cant_prom<<endl;

    system("pause");
    return 0;
}
