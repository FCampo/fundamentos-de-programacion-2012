#include <iostream>
#include <cstdlib>
#include <fstream>
#include <time.h>

using namespace std;

int main(int argc, char *argv[]) {

    srand(time(0));

    ofstream Esc("Notas.txt");

    int alumno[103][2];

    for (int i=0;i<103;i++){
        for (int e=0;e<2;e++){
            alumno[i][e]=1+rand()%99;
        }
    }

    for (int i=0;i<103;i++){
        Esc<<alumno[i][0]<<" "<<alumno[i][1]<<endl;
    }

    Esc.close();

    system("pause");
    return 0;
}
