/**
*
* Ejercicio 3;
*
* Cierto curso de matemática tiene una comisión de 103 alumnos. Escriba un algoritmo que:
*
*   *a) Guarde en cada fila de un arreglo bidimensional de 3 columnas, las notas correspondientes a los dos
*   parciales que se tomaron durante el cursado (en las columnas 1 y 2).
*   Para ello se ingresan, ordenadamente, nota del primer parcial y nota del segundo parcial de cada uno de los alumnos.
*
* El programa debe:
*
*   *b) Obtener el promedio para cada alumno y guardarlo en la tercera columna del arreglo.
*
*   *c) Guardar en otro arreglo la condición de cada alumno. Si es promocional (promedio >=75 puntos) guardar una 'P',
*   si es regular (promedio >=60 puntos) una 'R' y de otra forma, el alumno es libre y debe guardar una 'L'.
*
*   *d) Informar la cantidad de alumnos que promocionaron la materia.
*
*   (Leer los datos de un archivo txt);
**/

#include <iostream>
#include <cstdlib>
#include <fstream>
#include <iomanip>

using namespace std;

int main(int argc, char *argv[]) {

    ifstream Leer("Notas.txt");

    int Alumnos[103][3];
    char condicion[103];

    for (int i=0;i<103;i++){
        for (int e=0;e<2;e++){
            Leer>>Alumnos[i][e];
        }
    }

    for(int i=0;i<103;i++){
        Alumnos[i][2]=((Alumnos[i][0]+Alumnos[i][1])/2);
    }

    for (int i=0;i<103;i++){
        if(Alumnos[i][2]>=75){
            condicion[i]='P';
        }
        else if(Alumnos[i][2]>=60){
            condicion[i]='R';
        }
        else condicion[i]='L';
    }

    cout<<"(N1=Nota 1, N2=Nota 2, P=Promedio(P=Promocion, R=Regular, L=Libre), C=Condicion)"<<endl;
    cout<<"N1 N2 P C"<<endl<<endl;
    for (int i=0;i<103;i++){
        cout<<Alumnos[i][0]<<" "<<Alumnos[i][1]<<" "<<Alumnos[i][2]<<" "<<condicion[i]<<endl;
    }

    Leer.close();

    system("pause");
    return 0;
}
