/**
*Ejercicio 10
*
*Escriba un programa C++ que defina e inicialice un arreglo x de 10 enteros. El programa
*debe llamar a una funci�n nuevoarreglo() empleando como par�metro actual de llamada
*un arreglo p de punteros a los elementos del arreglo x.
*La funci�n debe retornar un nuevo arreglo con el doble de cada uno de los 10 datos
*apuntados por los elementos del arreglo p.
*
*/

#include <iostream>
#include <cstdlib>
#include <stdio.h>

using namespace std;

int *nuevoarreglo(int *p);

int main(int argc, char *argv[]) {
    int x[10];
    int *nue;
    nue=x;

    for (int i=0;i<10;i++){
        cin>>x[i];
    }

    nue=nuevoarreglo(x);

    for (int i=0;i<10;i++){
        cout<<nue[i]<<endl;
    }

    system("pause");
    return 0;
}

int *nuevoarreglo(int *p){
    int aux[10];
    int *n;
    n=aux;

    for(int i=0;i<10;i++){
        aux[i]=p[i];
    }
    for(int i=0;i<10;i++){
        aux[i]=(aux[i])*2;
    }
    n=aux;

    return n;
}
