/**
*
*Ejercicio 5
*
*A continuación se declara un arreglo x de 10
*elementos enteros. El elemento inicial x[0]se
*ubica en la dirección de memoria 0x11e4:
*
*int x[10]={110, 120, 130, 140, 150, 160, 170, 180, 190, 200};
*
*Determine el valor que representan las expresiones siguientes:
*
*a) x;
*b) (x+4);
*c) *x;
*d) *(x+3);
*e) *x+3;
*
*/

#include <iostream>
#include <cstdlib>

using namespace std;

int main(int argc, char *argv[]) {
    int x[10]={110, 120, 130, 140, 150, 160, 170, 180, 190, 200};

    cout<<"x: "<<x<<endl; //x=direccion de memoria;
    cout<<"(x+4): "<<(x+4)<<endl;   //(x+4)=direccion de memoria + 4;
    cout<<"*x: "<<*x<<endl; //*x=110;
    cout<<"*(x+3): "<<*(x+3)<<endl; //*(x+3)=140;
    cout<<"*x+3: "<<*x+3<<endl; //*x+3=113;

    system("pause");
    return 0;
}
