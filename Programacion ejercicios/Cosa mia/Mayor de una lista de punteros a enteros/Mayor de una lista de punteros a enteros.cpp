/**
*Ejercicio 11
*
*Escriba un programa C++ que invoque a una funci�n mayor() que acepte como
*par�metro un arreglo de n punteros a enteros y devuelva un puntero al mayor entero de la
*lista. En el main() muestre el mayor entero de la lista.
*
*/

#include <iostream>
#include <cstdlib>

using namespace std;

int *mayor(int *arre, int n);

int main(int argc, char *argv[]) {
    int tam;
    cout<<"cantidad de numeros enteros a ingresar:"<<ends;
    cin>>tam;
    int *arreglo;
    arreglo=new int [tam];
    int *mayorr;

    for (int i=0;i<tam;i++){
        cout<<"Ingrese el "<<i+1<<" entero:"<<ends;
        cin>>arreglo[i];
    }

    mayorr=mayor(arreglo, tam);

    cout<<"El mayor es: "<<*mayorr<<endl;

    delete[] arreglo;
    system("pause");
    return 0;
}

int *mayor(int *arre, int n){
    int *may;
    may=arre;

    for (int i=0;i<n;i++){
        if(*may<*(arre+i)){
            may=&arre[i];
        }
    }

    return may;
}
