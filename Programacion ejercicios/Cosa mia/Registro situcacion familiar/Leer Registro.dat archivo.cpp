/**
*
* Ejercicio 2 (PARTE 2);
*
* Determine y luego informe el porcentaje de mujeres y varones dentro de la comunidad,
* el promedio de miembros que conforman las familias, cuantos hogares tienen dos o m�s miembros con ingresos.
*
**/

#include <iostream>
#include <cstdlib>
#include <fstream>

using namespace std;

struct Familia{ //Declaro el struct Familia;
    int Miembros;
    int Menores_18;
    int Mayores_64;
    int Miembros_ingre;
    int Mujeres;
};

int main(int argc, char *argv[]) {

    fstream archi("Registro.dat", ios::in|ios::binary); //Abro el archivo como entrada y binario;

    int cant_mujeres=0, cant_varones, cant_miembros=0, familias_2ing=0;
    float porc_m, porc_v, prom_miembros;

    archi.seekg(0, ios::end); //Posiciono el puntero de lectura al final del archivo;
    int t=archi.tellg(); //Obtengo el tama�o del archivo;
    t=t/sizeof(Familia); //divido el tama�o del archivo por el tama�o del struct Familia y obtengo la cantidad de familias;
    archi.seekg(0, ios::beg); //Posiciono el puntero de lectura al inicio del archivo;
    Familia *Flia; //Creo un puntero a Familia; (pongo a Familia porque lo tomo como un nuevo tipo de dato asi como int, etc);
    Flia=new Familia [t]; //Creo un arreglo dinamico de tipo Familia;

    for (int i=0;i<t;i++){
        archi.read((char*)&Flia[i], sizeof(Familia)); //Leo el archivo almaceno los datos en el arreglo Flia;
    }

    for (int i=0;i<t;i++){
        cant_miembros+=Flia[i].Miembros; //Obtengo la cantidad de miembros;
        cant_mujeres+=Flia[i].Mujeres; //Obtengo la cantidad de mujeres;
    }
    cant_varones=cant_miembros-cant_mujeres; //Obtengo la cantidad de varones;

    porc_m=(cant_mujeres*100.0)/cant_miembros; //Obtengo el porcentaje de mujeres;
    porc_v=(cant_varones*100.0)/cant_miembros; //Obtengo el porcentaje de varones;
    prom_miembros=(cant_miembros*1.0/t); //Obtengo el promedio de miembros por familia;

    for (int i=0;i<t;i++){
        if (Flia[i].Miembros_ingre>=2){
            familias_2ing++; //Obtengo la cantidad de familias con 2 o mas ingresos;
        }
    }

    cout<<"Porcentaje de mujeres: "<<porc_m<<endl;
    cout<<"Porcentaje de varones: "<<porc_v<<endl;
    cout<<"Promedio de miembros por familia: "<<prom_miembros<<endl;
    cout<<"Cantidad de familias con 2 o mas personas con ingresos: "<<familias_2ing<<endl;

    archi.close(); //Cierro el archivo;

    delete[] Flia; //Elimino el arreglo de punteros a Familia;

    system("pause");
    return 0;
}
