/**
*
* Ejercicio 2 (PARTE 1);
*
* Se ha realizado un registro sobre la situaci�n familiar de un grupo de personas pertenecientes a una comunidad
* y se desean analizar los datos obtenidos.
* Escriba un programa en C++ que lea por pantalla y almacene los siguientes datos recabados de cada grupo familiar:
*
*   1* Miembros que conforman el hogar.
*   2* Cantidad de menores de 18.
*   3* Cantidad de personas con 65 a�os o m�s.
*   4* Cantidad de miembros con ingresos de alg�n tipo.
*   5* Total de femeninos.
*
*   (La lectura finaliza cuando se ingresa cero para la cantidad de miembros que conforman el hogar).
*   (El programa debe crear un archivo registro.dat con los datos ingresados).
**/

#include <iostream>
#include <cstdlib>
#include <fstream>

using namespace std;

struct Familia{
    int Miembros;
    int Menores_18;
    int Mayores_64;
    int Miembros_ingre;
    int Mujeres;
};

int main(int argc, char *argv[]) {

    fstream archi("Registro.dat", ios::out|ios::binary);

    Familia Flia;

    cout<<"Miembros del hogar (0 para finalizar el ingreso):"<<ends;
    cin>>Flia.Miembros;
    while (Flia.Miembros!=0){
        cout<<"Cantidad de miembros menores de 18 a�os:"<<ends;
        cin>>Flia.Menores_18;
        cout<<"Cantidad de miembros mayores de 65 a�os (inclusive 65):"<<ends;
        cin>>Flia.Mayores_64;
        cout<<"Cantidad de miembros que aportan ingresos a la familia:"<<ends;
        cin>>Flia.Miembros_ingre;
        cout<<"Cantidad de miembros femeninos:"<<ends;
        cin>>Flia.Mujeres;

        archi.write((char*)&Flia, sizeof(Familia));

        cout<<"Miembros del hogar (0 para finalizar el ingreso): "<<ends;
        cin>>Flia.Miembros;
    }

    archi.close();

    system("pause");
    return 0;
}
