#include <iostream>
#include <cstdlib>
#include <fstream>

using namespace std;

typedef struct {
    int a, b, c, d;
}racional;

int main(int argc, char *argv[]) {

    fstream archi("racionales.dat", ios::out|ios::binary);

    racional racio;
    racio.a=12;
    racio.b=6;
    racio.c=14;
    racio.d=7;

    archi.write((char*)&racio, sizeof(racional));

    archi.close();
    system("pause");
    return 0;
}
