#include <iostream>
#include <cstdlib>
#include <fstream>
#include <iomanip>

using namespace std;

typedef struct {
    int a, b;
}racional;

racional suma(racional r, racional r2);

int main(int argc, char *argv[]) {

    fstream archi("racionales.dat", ios::in|ios::binary);

    racional racio, racio2;

    archi.read((char*)&racio, sizeof(racional));
    archi.read((char*)&racio2, sizeof(racional));

    cout<<"Racional 1: "<<racio.a<<"/"<<racio.b<<endl;
    cout<<"Racional 2: "<<racio2.a<<"/"<<racio2.b<<endl;

    racional s;
    s=suma(racio, racio2);
    cout<<"Suma racional 1 + racional 2: "<<s.a<<"/"<<s.b<<endl;

    archi.close();
    system("pause");
    return 0;
}

racional suma(racional r, racional r2){
    racional sum;
    sum.a=((r.a*r2.b)+(r.b*r2.a));
    sum.b=(r.b*r2.b);

    for (int i=2;i<sum.b;i++){
        if(((sum.a%i)==0)&&((sum.b%i)==0)){
            sum.a=sum.a/i;
            sum.b=sum.b/i;
            break;
        }
    }
    return sum;
}
