/**
*
*size_t strlen(const char *c)
*
*Devuelve un entero con la longitud actual de la cadena argumento (sin incluir el
*car�cter de terminaci�n nulo).
*
*/

#include <iostream>
#include <cstdlib>

using namespace std;

size_t strlenn(const char *c);

int main(int argc, char *argv[]) {
    char cadena[10]="Internet";
    int x;

    x=strlenn(cadena);  //Llamada a la funcion strlen creada en el programa;

    cout<<x<<endl;

    system("pause");
    return 0;
}
size_t strlenn(const char *c){

    int cont=0;

    while(c[cont]!='\0'){   //Obtiene el tama�o de la cadena;
        cont++;
    }

    return cont;
}
