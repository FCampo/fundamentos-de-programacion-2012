/**
*
*char* strncat(char *c1,const char *c2, size_t n)
*
*Concatena (agrega) los n primeros caracteres de c2 a la variable c1. La funci�n
*devuelve la direcci�n de c1.
*
*/

#include <iostream>
#include <cstdlib>
#include <stdio.h>

using namespace std;

char* strncatt(char *c1,const char *c2, size_t n);

int main(int argc, char *argv[]) {

    char cadena1[10]="Internet";
    char cadena2[20]=" es la red de redes";

    strncatt(cadena1, cadena2, 10); //Llamada a la funcion strncat creada en el programa;

    puts(cadena1);

    system("pause");
    return 0;
}

char* strncatt(char *c1,const char *c2, size_t n){

    int cont1=0, e=0;

    while(c1[cont1]!='\0'){ //Obtiene el tama�o de la cadena1;
        cont1++;
    }

    for(int i=cont1;i<cont1+n;i++){ //Agrega los n primeros caracteres de c2 en c1;
        c1[i]=c2[e];
        e++;
    }

    c1[cont1+n]='\0'; //Agrega el fin de linea para completar la cadena;

    return c1;
}
