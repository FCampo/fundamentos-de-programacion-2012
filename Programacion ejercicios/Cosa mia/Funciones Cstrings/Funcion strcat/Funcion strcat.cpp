/**
*
*char* strcat(char *c1,const char *c2)
*
*Concatena (agrega) el cstring indicado como segundo par�metro (c2) al final de la
*variable cstring indicado como primer par�metro (c1). La funci�n devuelve el contenido
*de c1.
*
*/

#include <iostream>
#include <cstdlib>
#include <stdio.h>

using namespace std;

char* strcatt(char *c1,const char *c2);

int main(int argc, char *argv[]) {

    char cadena1[10]="Internet";
    char cadena2[20]=" es la red de redes";

    strcatt(cadena1, cadena2); //Llamada a la funcion strcat creada en el programa;

    puts(cadena1);

    system("pause");
    return 0;
}

char* strcatt(char *c1,const char *c2){

    int cont2=0, cont1=0, e=0;

    while(c1[cont1]!='\0'){ //Obtiene el tama�o de la cadena1;
        cont1++;
    }

    while(c2[cont2]!='\0'){ //Obtiene el tama�o de la cadena2;
        cont2++;
    }

    for(int i=cont1;i<cont2+cont1+1;i++){   //Agrega la cadena2 al final de cadena1;
        c1[i]=c2[e];
        e++;
    }

    return c1;
}
