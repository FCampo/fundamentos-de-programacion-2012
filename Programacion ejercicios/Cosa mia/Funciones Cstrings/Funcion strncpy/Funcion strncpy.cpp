/**
*
*char *strncpy(char *c1, const char *c2, size_t n)
*
*Esta funci�n copia n caracteres de c2 a c1 y devuelve un puntero a c1. Si n es menor
*que la longitud de la cadena destino (c1), esta conserva el resto de los caracteres
*iniciales; si n es mayor que c1 su longitud aumentar� para dar cabida a los n
*caracteres, pero debe agregarse el car�cter nulo �\0� para completar el cstring.
*
*/

#include <iostream>
#include <cstdlib>

using namespace std;

char *strncpyy(char *c1, const char *c2, size_t n);

int main(int argc, char *argv[]) {

    char x[19]="Pedro es ingeniero";
    char y[25]="Pablo es programador";

    cout<<strncpyy(x, y, 5)<<endl; //Llamada a la funcion strncpy creada en el programa;

    system("pause");
    return 0;
}

char *strncpyy(char *c1, const char *c2, size_t n){

    int cont=0;

    while (c1[cont]!='\0'){ //Obtiene el tama�o de la cadena c1;
        cont++;
    }

    for(int i=0;i<n;i++){   //Copia los n primeros caracteres de c2 en c1;
        c1[i]=c2[i];
    }

    if(n>cont){ //Si n es mayor al tama�o de la cadena 1, agrega el fin de linea para completar la cadena;
        c1[n]='\0';
    }

    return c1;
}
