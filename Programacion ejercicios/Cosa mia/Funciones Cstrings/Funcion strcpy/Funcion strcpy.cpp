/**
*
*char *strcpy(char *c1, const char *c2)
*
*Esta funci�n copia los caracteres de c2 a c1 y devuelve un puntero a c1. Podemos
*emplear esta funci�n para asignar una variable cstring a otra.
*
*/

#include <iostream>
#include <cstdlib>

using namespace std;

char *strcpyy(char *c1, const char *c2);

int main(int argc, char *argv[]) {

    char cadena1[40]="Internet es la red de redes";
    char cadena2[10]="la red";

    cout<<strcpyy(cadena1, cadena2)<<endl; //Llamada a la funcion strcpy creada en el programa;

    system("pause");
    return 0;
}

char *strcpyy(char *c1, const char *c2){

    int cont=0;

    while(c2[cont]!='\0'){  //Obtiene el tama�o de c2;
        cont++;
    }
    cont++; //aumenta en 1 el tama�o obtenido, para poder tener la cadena completa con el fin de linea inclusive;

    for (int i=0;i<cont;i++){   //Copia en c1 la cadena c2;
        c1[i]=c2[i];
    }

    return c1;
}
