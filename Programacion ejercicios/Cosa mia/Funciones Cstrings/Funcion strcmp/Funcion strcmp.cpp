/**
*
*int strcmp(const char *c1, const char *c2)
*
*Compara las cadenas y devuelve un entero menor que cero si c1 est� antes
*alfab�ticamente que c2, devuelve cero si las cadenas son iguales; y devuelve un
*entero mayor que cero si c2 est� antes. Veamos un ejemplo:
*
*/

#include <iostream>
#include <cstdlib>

using namespace std;

int strcmpp(const char *s1, const char *s2);

int main(int argc, char *argv[]) {

    char cadena1[20]="hola mundo";
    char cadena2[20]="hola world";
    char cadena3[20]="hola mundo";
    char cadena4[20]="aloha";
    int resultado;

    resultado=strcmpp(cadena1, cadena4); //Llamada a la funcion strcmp creada en el programa;

    cout<<resultado<<endl;

    system("pause");
    return 0;
}

int strcmpp(const char *s1, const char *s2){

    int resu;
    int cont=0;

    while(s1[cont]!='\0'){  //Obtiene el tama�o de la cadena s1;
        cont++;
    }

    for (int i=0;i<cont;i++){   //Compara la cadena s1 con la cadena s2;
        if (s1[i]==s2[i]){      //Si son iguales devuelve un 0;
            if (s1[i]=='\0'){
                resu=0;
                break;
            }
        }
        else{
            if (s1[i]<s2[i]){   //Si s1 esta antes alfabeticamente que s2 devuelve -1;
                resu=-1;
                break;
            }
            if(s1[i]>s2[i]){    //Si s2 esta antes alfabeticamente que s1 devulve 1;
                resu=1;
                break;
            }
        }
    }

    return resu;
}
