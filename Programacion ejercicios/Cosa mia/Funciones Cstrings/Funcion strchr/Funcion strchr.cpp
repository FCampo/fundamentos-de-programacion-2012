/**
*
*char* strchr(const char *c1,char c)
*
*Devuelve un puntero al inicio de la primera ocurrencia del car�cter c en c1. Devuelve
*NULL si no ocurre.
*
*/

#include <iostream>
#include <cstdlib>
#include <stdio.h>

using namespace std;

char* strchrr(const char *c1,char c);

int main(int argc, char *argv[]) {

    char cadena[40]="Internet es la red de redes";
    char* cad;

    cad=strchrr(cadena, 'd');   //Llamada a la funcion strchr creada en el programa;

    puts(cad);

    system("pause");
    return 0;
}

char* strchrr(const char *c1,char c){

    char *p;
    p=(char*)c1;
    int cont=0;

    while (c1[cont]!='\0'){ //Obtiene el tama�o de la cadena1;
        cont++;
    }

    for(int i=0;i<cont;i++){    //Compara la cadena con el caracter c,
        if(p[i]==c){            //luego devuelve un puntero a la primera coincidencia de la cadena con el caracter c;
            p=&p[i];
        }
    }

    if(p[0]!=c){    //Si no hubo coincidencias devuelve un puntero nulo;
        p=NULL;
    }

    return p;
}
