/**
*
*int strncmp(const char *c1, const char *c2, size_t n)
*
*Compara los primeros n caracteres de las cadenas empleadas como parámetros.
*Devuelve un entero menor que cero si los primeros n caracteres de c1 forman una
*subcadena que está antes alfabéticamente que los primeros n caracteres de c2,
*devuelve cero si las subcadenas son iguales; y devuelve un entero mayor que cero si
*la subcadena de c2 está antes que la de c1.
*
*/

#include <iostream>
#include <cstdlib>

using namespace std;

int strncmpp(const char *c1, const char *c2, size_t n);

int main(int argc, char *argv[]) {

    char cadena1[20]="hola mundo";
    char cadena2[20]="hola world";
    char cadena3[20]="hola mundano";
    char cadena4[20]="aloha";
    int resultado;

    resultado=strncmpp(cadena1, cadena3, 6);    //Llamada a la funcion strncmp creada en el programa;

    cout<<resultado<<endl;

    system("pause");
    return 0;
}

int strncmpp(const char *c1, const char *c2, size_t n){

    int cont=0;
    int resu;

    for(int i=0;i<n;i++){ //Compara los n primeros caracteres de la cadena 1 con la cadena 2;
        if(c1[i]==c2[i]){
            if(c1[n-1]==c2[n-1]){   //Si los n primeros caracteres de ambas cadenas son iguales devuelve un 0;
                resu=0;
            }
        }
        if(c1[i]<c2[i]){    //Si los n primeros caracteres de c1 estan alfabeticamente antes que los de c2 devuelve un -1;
            resu=-1;
            break;
        }
        if(c1[i]>c2[i]){    //Si los n primeros caracteres de c2 estan alfabeticamente antes que los de c1 devuelve un 1;
            resu=1;
            break;
        }
    }

    return resu;
}
