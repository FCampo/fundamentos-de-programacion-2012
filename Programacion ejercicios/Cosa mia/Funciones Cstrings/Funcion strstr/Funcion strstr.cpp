/**
*
*char* strstr(const char *c1, const char *c2)
*
*Devuelve un puntero al inicio de la primera ocurrencia de c2 en c1. Si no encuentra c1
*devuelve la direcci�n nula NULL.
*
*/

#include <iostream>
#include <cstdlib>
#include <stdio.h>

using namespace std;

char* strstrr(const char* c1, const char* c2);

int main(int argc, char *argv[]) {

    char cadena1[50];
    char cadena2[50];

    cout<<"Ingrese la cadena1: "<<endl;
    gets(cadena1);
    cout<<"Ingrese la cadena2: "<<endl;
    gets(cadena2);

    char* cad;
    cad=strstrr(cadena1, cadena2); //Llamada a la funcion strstr creada en el programa;

    puts(cad);

    system("pause");
    return 0;
}
char* strstrr(const char* c1, const char* c2){

    char* p;
    p=(char*)c1;    //Asigna al puntero p la direccion de memoria de la cadena 1 ((char*) es para que p no sea constante);
    int cont=0;
    int e=0;

    while (c1[cont]!='\0'){ //Obtiene el tama�o de la cadena 1;
        cont++;
    }

    for(int i=0;i<cont;i++){    //Compara p con c2;
        if(p[i]==c2[e]){
            e++;
            if (c2[e]=='\0'){
                p=&p[i-e+1];    //Asigna a p el inicio de la primera ocurrencia de c2 en c1;
                break;  //Termina el ciclo for cuando se encuentra la ocurrencia;
            }
            continue;   //Si encuentra ocurrencia salta la linea 55 y sigue comparar p con c2;
        }
        e=0; //Si hubo caracteres diferentes vuelve comparar p (desde donde esta) con c2 (desde el inicio);
    }

    if(p==c1){  //Si no hay ocurrencia devuelve un puntero nulo;
        p=NULL;
    }

    return p;
}
