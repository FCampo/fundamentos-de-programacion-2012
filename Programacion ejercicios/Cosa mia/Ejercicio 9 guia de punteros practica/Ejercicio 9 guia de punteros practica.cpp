/**
*
*Ejercicio 9
*
*Escriba una funci�n que utilice punteros para buscar e informar la direcci�n de un entero
*dentro de un arreglo. Se pasan como par�metros el arreglo y el entero a buscar. Si el dato
*no se encuentra informar la direcci�n nula (NULL).
*
*/

#include <iostream>
#include <cstdlib>

using namespace std;

int* busca_entero(int arr[], int buscar);

int main(int argc, char *argv[]) {

    int arre[10]={0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
    int b_entero=4;
    cout<<&arre[4]<<endl;
    int* puntero;

    puntero=busca_entero(arre, b_entero);

    cout<<"Direccion del numero buscado: "<<puntero<<endl;

    system("pause");
    return 0;
}

int* busca_entero(int arr[], int buscar){
    int *p;
    for (int i=0;i<10;i++){
        if(arr[i]==buscar){
            p=&arr[i];
            break;
        }
        else p=NULL;
    }

    return p;
}
