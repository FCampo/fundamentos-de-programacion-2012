/*
Mar�a debe completar el tablero de 2x3 de la figura con 5 n�meros enteros positivos,
uno en cada casilla libre (la del centro abajo ya tiene 84), de manera que en cada columna,
el n�mero de arriba es m�ltiplo del de abajo, y el n�mero que aparece en cada casilla se
obtiene sum�ndole 10 al n�mero de la casilla inmediata de su izquierda. Los 6 n�meros
son distintos y menores a 500000. �Hay una �nica forma de llenarlo?
x  x  x
x  84 x
*/

#include <iostream>
#include <cstdlib>
#include <time.h>

using namespace std;

int main(int argc, char *argv[]) {
    long tabla[2][3];
    int f;

    tabla[1][1]=84;
    tabla[1][2]=94;
    tabla[1][0]=74;

    cout<<"Maria debe completar el tablero de 2x3 de la figura con 5 numeros enteros \npositivos, ";
    cout<<"uno en cada casilla libre (la del centro abajo ya tiene 84), de \nmanera que en cada columna, ";
    cout<<"el numero de arriba es m�ltiplo del de abajo, y el \nnumero que aparece en cada casilla se ";
    cout<<"obtiene sum�ndole 10 al numero de la \ncasilla inmediata de su izquierda. \nLos 6 numeros ";
    cout<<"son distintos y menores a 500000. Hay una unica forma de llenarlo?\n";
    cout<<"x  x  x\n";
    cout<<"x  84 x\n\n";

    cout<<"hay 4 formas de llenarlo: "<<endl<<endl<<"1) \n";

    for (int i=0;i<500000;i++){
        if((i%84)==0){
            if(((i+10)%94)==0){
                if(((i-10)%74)==0){
                    tabla[0][0]=i-10;
                    tabla[0][1]=i;
                    tabla[0][2]=i+10;
                    f=i+10;
                    break;
                }
            }
        }
    }
    for(int i=0; i<2;i++){
        for(int e=0;e<3;e++){
            cout<<tabla[i][e]<<ends;
        }
        cout<<endl;
    }
    cout<<endl<<"2) \n";

    for (int i=f;i<500000;i++){
        if((i%84)==0){
            if(((i+10)%94)==0){
                if(((i-10)%74)==0){
                    tabla[0][0]=i-10;
                    tabla[0][1]=i;
                    tabla[0][2]=i+10;
                    f=i+10;
                    break;
                }
            }
        }
    }
    for(int i=0; i<2;i++){
        for(int e=0;e<3;e++){
            cout<<tabla[i][e]<<ends;
        }
        cout<<endl;
    }
    cout<<endl<<"3) \n";

    for (int i=f;i<500000;i++){
        if((i%84)==0){
            if(((i+10)%94)==0){
                if(((i-10)%74)==0){
                    tabla[0][0]=i-10;
                    tabla[0][1]=i;
                    tabla[0][2]=i+10;
                    f=i+10;
                    break;
                }
            }
        }
    }
    for(int i=0; i<2;i++){
        for(int e=0;e<3;e++){
            cout<<tabla[i][e]<<ends;
        }
        cout<<endl;
    }
    cout<<endl<<"4) \n";

    for (int i=f;i<500000;i++){
        if((i%84)==0){
            if(((i+10)%94)==0){
                if(((i-10)%74)==0){
                    tabla[0][0]=i-10;
                    tabla[0][1]=i;
                    tabla[0][2]=i+10;
                    f=i+10;
                    break;
                }
            }
        }
    }
    for(int i=0; i<2;i++){
        for(int e=0;e<3;e++){
            cout<<tabla[i][e]<<ends;
        }
        cout<<endl;
    }
    cout<<endl;

    system("pause");
	return 0;
}

