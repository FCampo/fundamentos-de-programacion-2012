#include <iostream>
#include <cstdlib>

using namespace std;

int main(int argc, char *argv[]) {
    int cont;
    int divisores[200];

    for (int i=1;i<100000;i++){
        cont=0;
        for (int j=1;j<i+1;j++){
            if((i%j)==0){
                cont++;
                divisores[cont-1]=j;
            }
        }
        if (cont>=119){
            cout<<i<<" tiene "<<cont<<" divisores"<<endl<<"Sus divisores son:"<<endl;
            for (int p=0;p<cont;p++){
                cout<<divisores[p]<<ends;
            }
            cout<<endl<<endl;
        }
    }


    system("pause");
	return 0;
}

