#include <iostream>
#include <cstdlib>
#include <locale.h>
#include <cmath>

using namespace std;

int main(int argc, char *argv[]) {

    float x = 4.5, y = 12.3;
    int z = 10, u = 8;
    char letra='m';

    cout<<"a) ++u/2: (4)"<<endl;//aca u toma el valor 9.
    cout<<++u/2<<endl;

    cout<<"b) letra+2: (111)"<<endl;//aca se suma el numero asqui del valor de letra (m=109. 109+2=111. pero letra sigue siendo m).
    cout<<letra+2<<endl;

    cout<<"c) (x-y)/2+abs(u-y): (-0,6)"<<endl;//u esta valiendo 9.
    cout<<(x-y)/2+abs(u-y)<<endl;

    cout<<"d) 3+u%3: (3)"<<endl;//u esta valiendo 9.
    cout<<3+u%3<<endl;

    cout<<"e) u++/2: (4)"<<endl;
    cout<<u++/2<<endl;//aca u sigue valiendo 9 para despues valer 10.

    cout<<"f) 2*--u+x/3: (19,5)"<<endl;//aca u que vale 10 pasa a valer 9.
    cout<<2*--u+x/3<<endl;

    cout<<"g) letra+=1: (n)"<<endl;//aca letra cambia (letra=letra+1).
    letra+=1;
    cout<<letra<<endl;

    cout<<"h) pow(z,3): (1000)"<<endl;
    cout<<pow(z,3)<<endl;

    cout<<"i) x=(x+1)/2: (2,75)"<<endl;//aca x cambia.
    x=(x+1)/2;
    cout<<x<<endl;

    cout<<"j) u+=(z<100): (10)"<<endl;// aca u vale 9 y luego pasa a valer 10.
    u+=(z<100);
    cout<<u<<endl;

    system("pause");
	return 0;
}

