#include <iostream>
#include <cstdlib>
#include <math.h>

using namespace std;

int determinante(float **mat, int m);
int determninante2x2(float **matri);
int determinante3x3(float **matrix, int b);
int determinante4x4(float **matr, int cant);

int main(int argc, char *argv[]) {

    int n;
    char opc;

    cout<<"Tama�o de la matriz cuadrada (2;3;4):"<<ends;
    cin>>n;
    while ((n>4)||(n<2)){
        cout<<"Tama�o de la matriz cuadrada (2;3;4):"<<ends;
        cin>>n;
    }
    if (n==4){
        cout<<"Si la matriz tiene un 0 en la esquina superior derecha, primero reduscala a una de 3x3 y luego ingrese la matriz de 3x3"<<endl;
        cout<<"Tiene un 0 en la esquina superior derecha y ya la convirtio en una de 3x3? s=si, n=no"<<ends;
        cin>>opc;
        if (opc=='s'){
            n=3;
            cout<<"Ingrese la matriz de 3x3"<<endl;
        }
        else exit(1);
    }

    float **matriz;
    matriz=new float *[n];
    for (int i=0;i<n;i++){
        matriz[i]=new float [n];
    }

    cout<<endl<<"Llenar la matriz por filas (a11 a12 a13 ... a1n)"<<endl<<endl;
    for (int i=0;i<n;i++){
        for (int e=0;e<n;e++){
            cout<<"a"<<i+1<<e+1<<ends;
            cin>>matriz[i][e];
        }
    }

    for (int i=0;i<n;i++){
        for (int e=0;e<n;e++){
            cout<<matriz[i][e]<<ends;
        }
        cout<<endl;
    }

    int det;
    det=determinante(matriz, n);

    cout<<"Determinante: "<<det<<endl;

    for(int i = 0; i < n; i++){
		delete[] matriz[i];
	}
	delete[] matriz;
    system("pause");
	return 0;
}

int determninante2x2(float **matri){
    int determin=0;

    determin=(matri[0][0]*matri[1][1]-matri[0][1]*matri[1][0]);

    return determin;
}
int determinante3x3(float **matrix, int b){
    int determinan=0, p;

    for (int i=0;i<b;i++){
            if(i==0){
                determinan=determinan+(matrix[0][i]*((matrix[i+1][i+1]*matrix[i+2][i+2])-(matrix[i+1][i+2]*matrix[i+2][i+1])));
            }
            else
                if ((i>0)&&(i<(b-1))){
                    determinan=determinan-(matrix[0][i]*((matrix[i][i-1]*matrix[i+1][i+1])-(matrix[i][i+1]*matrix[i+1][i-1])));
                }
                    else if (i==(b-1)){
                            determinan=determinan+(matrix[0][i]*((matrix[i-1][i-2]*matrix[i][i-1])-(matrix[i-1][i-1]*matrix[i][i-2])));}
    }

    return determinan;
}
int determinante4x4(float **matr, int cant){
    int determ=0;

    float **aux;
    aux=new float *[cant-1];
    for (int i=0;i<cant;i++){
        aux[i]=new float [cant-1];
    }
    for (int k=0;k<cant;k++){
        for (int i=0;i<cant-1;i++){
            for (int j=0;j<cant-1;j++){
                if(k==0){
                    aux[i][j]=matr[i+1][j+1];
                } else
                if(k==1){
                    if (j==0){
                        aux[i][j]=matr[i+1][j];
                    }
                    else if(j==1){
                            aux[i][j]=matr[i+1][j+1];
                        }
                        else aux[i][j]=matr[i+1][j+2];
                } else
                if (k==2){
                    if ((j==0)||(j==1)){
                        aux[i][j]=matr[i+1][j];
                    }
                    else if(j==2){
                        aux[i][j]=matr[i+1][j+1];
                    }
                } else
                if (k==3){
                    aux[i][j]=matr[i+1][j];
                }
            }
        }
        determ=determ+(determinante3x3(aux, cant-1)*matr[0][k]*(pow(-1,(k+2))));
    }
    for(int i = 0; i < cant-1; i++){
        delete[] aux[i];
    }
    delete[] aux;

    return determ;
}
int determinante(float **mat, int m){
    int deter=0;
    if (m==2){
        deter=determninante2x2(mat);
    }
    if (m==3){
        deter=determinante3x3(mat, m);
    }
    if (m==4){
        deter=determinante4x4(mat, m);
    }
    return deter;
}
