
/*Ejercicio 6: La funci�n de C++: int max2(�) devuelve un entero que representa la frecuencia de aparici�n del mayor valor
en un arreglo de enteros: si el mayor valor es �nico devuelve 1, si se encuentra 2 veces, devuelve 2, etc.
La funci�n tambi�n permite obtener el valor del mayor elemento del arreglo. El arreglo y su longitud son par�metros de la funci�n.
a) Escriba el c�digo C++ de la funci�n max2 proponiendo los par�metros necesarios.
b) Escriba solo el prototipo de una sobrecarga de la funci�n max2 de modo que devuelva el mayor y el menor valor del arreglo,
y sus respectivas frecuencias de aparici�n.
c) Escriba un peque�o programa C++ cliente de la funci�n max2(...) que propuso en (a).*/

#include <iostream>
#include <cstdlib>
#include <time.h>

using namespace std;

int max2(int vect[], int longi, int &maxi);
int max2(int vec[], int lon, int maximo, int &minimo, int &frecmax, int &frecmin);

int main(int argc, char *argv[]) {
    srand(time(0));
    int n=150, mayor=0, menor=0, vecto[n], repeticiones, frecuenciamin=0, frecuenciamax=0, opc;

    for (int i=0;i<n;i++){
        vecto[i]=1+rand()%150;
    }

    for (int i=0;i<n;i++){
        cout<<vecto[i]<<ends;
    }
    cout<<endl;

    cout<<"Si desea obtener el valor mayor y su frecuencia de repeticion presone 1,\n";
    cout<<"si desea obtener el valor mayor, el menor y la frecuancia de repeticion de cada uno presione 2:"<<ends;
    cin>>opc;
    cout<<endl;

    switch (opc){
        case 1: repeticiones=max2(vecto, n, mayor);
                cout<<"El mayor es: "<<mayor<<endl;
                cout<<"Cantidad de repeticiones del mayor: "<<repeticiones<<endl;
                break;
        case 2: mayor=max2(vecto, n, mayor, menor, frecuenciamax, frecuenciamin);
                cout<<"El mayor es: "<<mayor<<endl;
                cout<<"cantidad de repeticiones del mayor: "<<frecuenciamax<<endl;
                cout<<"El menor es: "<<menor<<endl;
                cout<<"cantidad de repeticiones del menor: "<<frecuenciamin<<endl;
                break;
    }

    system("pause");
	return 0;
}

int max2(int vect[], int longi, int &maxi){
    int rep=0;

    maxi=vect[0];

    for (int i=0;i<longi;i++){
        if (maxi<vect[i]){
            maxi=vect[i];
        }
    }
    for(int i=0;i<longi;i++){
        if (maxi==vect[i]){
            rep++;
        }
    }

    return rep;
}

int max2(int vec[], int lon, int maximo, int &minimo, int &frecmax, int &frecmin){

    maximo=vec[0];
    minimo=vec[0];

    for (int i=0;i<lon;i++){
        if (maximo<vec[i]){
            maximo=vec[i];
        }
    }
    for(int i=0;i<lon;i++){
        if (maximo==vec[i]){
            frecmax++;
        }
    }

    for (int i=0;i<lon;i++){
        if(minimo>vec[i]){
            minimo=vec[i];
        }
    }
    for(int i=0;i<lon;i++){
        if (minimo==vec[i]){
            frecmin++;
        }
    }

    return maximo;
}
