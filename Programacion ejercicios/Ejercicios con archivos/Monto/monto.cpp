/*Ejercicio 1: Complete un programa C++ que permita ingresar los aportes voluntarios
realizados por los alumnos de una Facultad a su Asociaci�n Cooperadora.
Se desea realizar un an�lisis estad�stico de los aportes por curso (1..5) y por
cada mes del ciclo lectivo 2009 (1..12). Se ingresan 4 n�meros por cada aportante:
DNI del aportante, Curso del aportante [1..5], Mes [1..12], monto aportado.
Los montos no son fijos y son propuestos a voluntad por el aportante.
El programa debe:
a) Informar el curso que mayor monto aport� durante 2009.
b) Obtener e informar la recaudaci�n mensual de la Cooperadora.
c) Generar un archivo de textos con los datos de los aportes de cada curso;
en cada l�nea del archivo debe figurar: el Nro de curso y la lista con los
12 montos aportados durante el 2009 por ese curso. Ud. debe emplear el cuerpo
del programa propuesto en el recuadro completando lo que crea necesario.*/

#include <iostream>
#include <fstream>
#include <cstdlib>
using namespace std;

struct persona {
       int dni;
       int curso;
       float monto[12];
       };

int CursoMayorMonto (persona alm[], int n);
void RecaudacionMensual (persona almn[], int t, float totm[], int l);

int main(int argc, char *argv[]) {
	persona alumno[100];
	int cmm;
	int e=0;
	int opcion=1;
	int mes;
	float totalm [12]={0};
	float cm[5][12]={0};
	ofstream esc("monto.txt");

    for (int i=0; i<100; i++){
        for (int j=0; j<12; j++){
            alumno[i].monto[j]=0;
        }
    }

	while (opcion==1){
          cout<<"ingrese dni, curso, mes y monto del alumno"<<endl;
          cin>>alumno[e].dni;
          cin>>alumno[e].curso>>mes;
          cin>>alumno[e].monto[mes-1];
          cout<<"ingresar otro alumno? 1=si 0=no"<<endl;
          cin>>opcion;
          e++;
          }

    int i=0;

    while (i<e){
        for (int k=0; k<12; k++){
            cm[alumno[i].curso-1][k]+=alumno[i].monto[k];
        }
        i++;
    }

    cmm=CursoMayorMonto(alumno, e);
    RecaudacionMensual(alumno, e, totalm, 12);
    cout<<"curso mayor monto: "<<cmm<<endl;
    for (int i=0; i<12; i++){
        cout<<"monto total en mes "<<i+1<<": "<<totalm[i]<<endl;
        }
    for (int i=0; i<5; i++){
        esc<<"curso: "<<i+1<<endl;
        for (int k=0; k<12; k++){
            esc<<cm[i][k]<<endl;
        }
    }

    esc.close();
	system ("pause");
    return 0;
}

int CursoMayorMonto (persona alm[], int n){
    float acum1=0, acum2=0, acum3=0, acum4=0, acum5=0;
    for (int i=0; i<n; i++){
        if (alm[i].curso==1){
           for(int m=0; m<12; m++){
                   acum1+=alm[i].monto[m];
           }
        }
        if (alm[i].curso==2){
           for(int m=0; m<12; m++){
                   acum2+=alm[i].monto[m];
           }
        }
        if (alm[i].curso==3){
           for(int m=0; m<12; m++){
                   acum3+=alm[i].monto[m];
           }
        }
        if (alm[i].curso==4){
           for(int m=0; m<12; m++){
                   acum4+=alm[i].monto[m];
           }
        }
        if (alm[i].curso==5){
           for(int m=0; m<12; m++){
                   acum5+=alm[i].monto[m];
           }
        }
    }
    if ((acum1>=acum2)&&(acum1>=acum3)&&(acum1>=acum4)&&(acum1>=acum5)){
       return 1;
       }
    if ((acum2>=acum1)&&(acum2>=acum3)&&(acum2>=acum4)&&(acum2>=acum5)){
       return 2;
       }
    if ((acum3>=acum1)&&(acum3>=acum2)&&(acum3>=acum4)&&(acum3>=acum5)){
       return 3;
       }
    if ((acum4>=acum1)&&(acum4>=acum2)&&(acum4>=acum3)&&(acum4>=acum5)){
       return 4;
       }
    if ((acum5>=acum1)&&(acum5>=acum2)&&(acum5>=acum3)&&(acum5>=acum4)){
       return 5;
       }
}

void RecaudacionMensual (persona almn[], int t, float totm[], int l){
     for (int j=0; j<l; j++){
         for (int i=0; i<t; i++){
             totm[j]+=almn[i].monto[j];
         }
     }
}
