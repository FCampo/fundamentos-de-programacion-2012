/*Ejercicio 5: La Secretar�a Nacional de Sanidad Animal (SENASA) desea analizar los casos de aftosa en bovinos producidos
en 2010 en Argentina.
Para esto se desea emplear un programa C++ que organice en un matriz de 23 filas (provincias) x 12 columnas (meses del 2010).
El programa accede a un archivo de textos DATA2010.TXT que contiene en cada l�nea 3 valores con el reporte de cada caso enviado
por las delegaciones de SENASA de cada provincia en todo 2010: C�digo de Pcia, Mes, Nro de casos reportados.
En una misma provincia y un mismo mes pueden ingresar varios reportes. El programa debe determinar e informar:
a) El total de casos registrados en cada provincia en todo 2010;
b) En qu� provincia se produjo la mayor cantidad de casos en todo el a�o y dicha cantidad;
c) en cu�ntos meses la provincia 17 tuvo 0 casos. Emplee funciones.*/
#include <iostream>
#include <cstdlib>
#include <fstream>

using namespace std;

void cant_prov(int dat[][12], int can[]);//cantidad de casos por provincias.

int mayor_cantp(int cantd[], int &pro);//provincia con mayor cantidad de casos y la cantidad.

int cant_meses(int prov17[]);//cantidad de meses en que la prov 17 tuvo 0 casos.

int main(int argc, char *argv[]) {
    int datos[23][12]={0};
    int cantc[23]={0};
    int provincia17[12];
    int mes, prov, cantidad;
    ifstream leer("data2010.txt");
    while (leer>>prov){
        leer>>mes>>cantidad;
        datos[prov][mes]+=cantidad;
    }
    for (int i=0;i<23;i++){
        cout<<"depto "<<i+1<<":"<<ends;
        for (int e=0;e<12;e++){
            cout<<"mes "<<e+1<<":"<<ends;
            cout<<datos[i][e]<<ends;
        }
        cout<<endl;
    }
    cout<<endl;

    //punto a)
    cant_prov(datos, cantc);
    for (int i=0;i<23;i++){
        cout<<"cantidad de casos en provincia "<<i+1<<": "<<cantc[i]<<endl;
    }
    cout<<endl;

    //punto b)
    int mayorp=mayor_cantp(cantc, prov);
    cout<<"en la provincia "<<prov+1<<" es en la que mayor cantidad de casos hubo: "<<mayorp<<endl<<endl;

    //punto c)
    for (int i=0;i<12;i++){
        provincia17[i]=datos[16][i];
    }
    int cant=cant_meses(provincia17);
    cout<<"cantidad de meces que la provincia 17 no tuvo casos: "<<cant<<endl<<endl;

    leer.close();
    system("pause");
	return 0;
}

void cant_prov(int dat[][12], int can[]){
    for (int i=0;i<23;i++){
        for (int e=0;e<12;e++){
            can[i]+=dat[i][e];
        }
    }
}//cantidad de casos por provincias.

int mayor_cantp(int cantd[], int &pro){
    int mayor=cantd[0];
    for(int i=0;i<23;i++){
        if (mayor<cantd[i]){
            mayor=cantd[i];
            pro=i;
        }
    }
    return mayor;
}//provincia con mayor cantidad de casos y la cantidad.

int cant_meses(int prov17[]){
    int cont=0;
    for(int i=0;i<12;i++){
        if(prov17[i]==0){
            cont++;
        }
    }
    return cont;
}//cantidad de meses en que la prov 17 tuvo 0 casos.
