/*Ejercicio 2: Un archivo binario llamado E4STRUCT.DAT contiene un conjunto de structs cuyos miembros
son {int x,y; char a[40]; float h;}, escriba un programa C++ acceda al archivo y
a trav�s de una funci�n identifique el struct con el mayor valor de h y el struct del
menor valor de h, luego ubique ambos registros al inicio (el mayor h) y al final haciendo los intercambios necesarios.
Proponga los par�metros que crea necesarios.*/
#include <iostream>
#include <fstream>
#include <cstring>
#include <cstdlib>
using namespace std;

struct personas{
       int x;
       int y;
       char a[40];
       float h;};
personas busca_mayorh(personas perso[], int n);
personas busca_inh(personas person[], int na);
int main(int argc, char *argv[]) {
    fstream archi("e4struct.dat", ios::in|ios::out|ios::binary);
    archi.seekg(0, ios::end);
    int t=archi.tellg();
    t=(t/sizeof(personas));
    personas p, hmin, hax;
    personas pers[t];
    archi.seekg(0, ios::beg);

    for (int i=0;i<t;i++){
        archi.read((char*)&p, sizeof(p));
        pers[i]=p;
    }
    archi.seekg(0, ios::beg);
    for (int i=0;i<t;i++){
        cout<<pers[i].x<<" "<<pers[i].y<<endl;
        puts(pers[i].a);
        cout<<pers[i].h<<endl;
    }
    cout<<endl;

    hax=busca_mayorh(pers, t);
    cout<<"persona con el h mas grande: "<<endl;
    cout<<hax.x<<" "<<hax.y<<endl;
    puts(hax.a);
    cout<<hax.h<<endl;
    cout<<endl;

    hmin=busca_inh(pers, t);
    cout<<"persona con el h mas chico: "<<endl;
    cout<<hmin.x<<" "<<hmin.y<<endl;
    puts(hmin.a);
    cout<<hmin.h<<endl;
    cout<<endl;

    for (int i=0;i<t;i++){
        if (pers[i].h==hax.h){
            p=pers[0];
            pers[0]=pers[i];
            pers[i]=p;
        }
        else {
            if(pers[i].h==hmin.h){
                p=pers[t-1];
                pers[t-1]=pers[i];
                pers[i]=p;
            }
        }
    }

    for (int i=0;i<t;i++){
        archi.write((char*)&pers[i], sizeof(p));
    }
    for (int i=0;i<t;i++){
        cout<<pers[i].x<<" "<<pers[i].y<<endl;
        puts(pers[i].a);
        cout<<pers[i].h<<endl;
        cout<<endl;
    }

    archi.close();
    system("pause");
	return 0;
}

personas busca_mayorh(personas perso[], int n){
         float hmax=0;
         int persomax;
         for (int i=0;i<n;i++){
             if (perso[i].h>hmax){
                hmax=perso[i].h;
                persomax=i;}
         }
         return perso[persomax];
}

personas busca_inh(personas person[], int na){
         float hmin=100;
         int persomin;
         for (int i=0;i<na;i++){
             if (person[i].h<hmin){
                hmin=person[i].h;
                persomin=i;}
         }
         return person[persomin];
}
