/**
* Ejercicio 11
*
* Un arreglo E4STRUCT contiene un conjunto de structs cuyos miembros son {int x,y; char a[40]; float h;},
* escriba un programa C++ acceda al arreglo e identifique el struct con el mayor valor de h y el struct del menor valor de h,
* luego ubique ambos elementos al inicio (el de mayor h) y al final haciendo los intercambios necesarios.
* Proponga la lectura de los datos para E4STRUCT.
*
*/

#include <iostream>
#include <cstdlib>
#include <fstream>
#include <stdio.h>

using namespace std;

struct persona{
    int x, y;
    char a[40];
    float h;
};

int mayor_h(persona l[], int tam);

int menor_h(persona f[], int tam);

int main(int argc, char *argv[]) {
    fstream archi("e4struct.dat", ios::in|ios::out|ios::binary);
    archi.seekg(0, ios::end);
    int tamanio=archi.tellg();
    tamanio=tamanio/sizeof(persona);
    persona *p;
    p=new persona [tamanio];
    archi.seekg(0, ios::beg);
    for (int i=0;i<tamanio;i++){
        archi.read((char*)&p[i], sizeof(persona));
    }
    archi.seekg(0, ios::beg);

    for (int i=0;i<tamanio;i++){
        cout<<"x: "<<p[i].x<<" y: "<<p[i].y<<endl;
        cout<<"Nombre:"<<ends;
        puts(p[i].a);
        cout<<"h: "<<p[i].h<<endl;
        cout<<"----------"<<endl;
    }
    int h_max=mayor_h(p, tamanio);

    int h_min=menor_h(p, tamanio);

    cout<<endl<<"<<<<<<<<<"<<endl;
    cout<<"x: "<<p[h_max].x<<" y: "<<p[h_max].y<<endl;
    cout<<"Nombre:"<<ends;
    puts(p[h_max].a);
    cout<<"h: "<<p[h_max].h<<endl;
    cout<<"<<<<<<<<<"<<endl;

    cout<<"x: "<<p[h_min].x<<" y: "<<p[h_min].y<<endl;
    cout<<"Nombre:"<<ends;
    puts(p[h_min].a);
    cout<<"h: "<<p[h_min].h<<endl;
    cout<<"<<<<<<<<<"<<endl<<endl;

    persona aux;
    aux=p[h_max];
    p[h_max]=p[0];
    p[0]=aux;

    aux=p[h_min];
    p[h_min]=p[tamanio-1];
    p[tamanio-1]=aux;

    for (int i=0;i<tamanio;i++){
        archi.write((char*)&p[i], sizeof(persona));
    }
    for (int i=0;i<tamanio;i++){
        cout<<"x: "<<p[i].x<<" y: "<<p[i].y<<endl;
        cout<<"Nombre:"<<ends;
        puts(p[i].a);
        cout<<"h: "<<p[i].h<<endl;
        cout<<"----------"<<endl;
    }

    cout<<"SI NO QUEDO EL MAXIMO VALOR DE H AL PRINCIPIO NI EL MINIMO VALOR DE H AL FINAL"<<endl;
    cout<<"VOLVER A CORRER EL PROGAMA!!!"<<endl;

    archi.close();
    delete[] p;
    system("pause");
    return 0;
}

int mayor_h(persona l[], int tam){
    int hmax;

    hmax=0;
    for (int i=0;i<tam;i++){
        if(l[hmax].h<l[i].h){
            hmax=i;
        }
    }

    return hmax;
}

int menor_h(persona f[], int tam){
    int hmin=tam-1;
    for (int i=tam-1;i>=0;i--){
        if(f[hmin].h>f[i].h){
            hmin=i;
        }
    }
    return hmin;
}
