/*Ejercicio 4: Se dispone de un archivo de texto CALIFIC.TXT que contiene una serie de strings. Cada string se conforma con el DNI,
las calificaciones parciales (3 por alumno) y el nombre de un grupo de estudiantes que cursaron una asignatura. Escriba una aplicaci�n
C++ que haga lo siguiente:
a) Lea los datos del archivo y organ�celos en un arreglo: cada elemento del arreglo deben contener todos los datos de un alumno.
b) Dise�e la funci�n max2(  ) que reciba el arreglo como par�metro y devuelva los 2 alumnos con mejor promedio de la asignatura.
Proponga los par�metros necesarios.
c) Complete el programa, el cual debe invocar a la funci�n max2(  ) y mostrar los nombres, DNI y
promedios de los 2 alumnos de mejor desempe�o.*/

#include <iostream>
#include <fstream>
#include <cstdlib>
#include <cstring>
using namespace std;

struct persona{
    int dni, n1, n2, n3;
    char nombre[50];
};

persona max2(persona pers[], int l, persona &m2);

int main(int argc, char *argv[]) {
    ifstream leer ("calific.txt");
    persona p, per[100], p1max, p2max;
    int i=0;

    while(leer>>p.dni){
        leer>>p.n1>>p.n2>>p.n3;
        leer.ignore();
        leer.getline(p.nombre, 50);
        per[i]=p;
        i++;
    }
    for (int e=0;e<i;e++){
        cout<<per[e].dni<<" "<<per[e].n1<<" "<<per[e].n2<<" "<<per[e].n3<<ends;
        puts(per[e].nombre);
        cout<<endl;
    }

    p1max=max2(per, i, p2max);

    cout<<"-----------------o-----------------"<<endl;

    cout<<"alumno con promedio mas alto: "<<p1max.nombre<<endl;
    cout<<"D.N.I: "<<p1max.dni<<endl;
    float promedio=((p1max.n1+p1max.n2+p1max.n3)/3.0);
    cout<<"promedio: "<<promedio<<endl;

    cout<<"-----------------o-----------------"<<endl;

    cout<<"alumno con el segundo promedio mas alto: "<<p2max.nombre<<endl;
    cout<<"D.N.I: "<<p2max.dni<<endl;
    float promedio2=((p2max.n1+p2max.n2+p2max.n3)/3.0);
    cout<<"promedio: "<<promedio2<<endl;

    cout<<"-----------------o-----------------"<<endl;
    leer.close();
    system("pause");
	return 0;
}

persona max2(persona pers[], int l, persona &m2){
    persona m1;
    float prome[l];
    for (int k=0;k<l;k++){
        prome[k]=((pers[k].n1+pers[k].n2+pers[k].n3)/3.0);
    }
    float promed=0;
    m1=pers[0];
    int ma;
    for (int k=0;k<l;k++){    //No se pude hacer el max promedio 1 y el max promedio 2 en el mismo ya que si lo hacia
        if (promed<prome[k]){ //si el promedio empezaba con el maximo no volvia a entrar al if y no conseguia el segundo promedio
            m1=pers[k];       //guardaba el mismo alumno.
            ma=k;
            promed=prome[k];
        }
    }
    promed=0;
    for (int k=0;k<l;k++){
        if(k!=ma){
            if(promed<prome[k]){
                m2=pers[k];
                promed=prome[k];
            }
        }
    }
    return m1;
}
