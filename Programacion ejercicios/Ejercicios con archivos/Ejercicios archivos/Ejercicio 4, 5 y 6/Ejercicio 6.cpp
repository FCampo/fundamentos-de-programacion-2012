/*
Ejercicio 6
Dise�e funciones que operen sobre el archivo grupo.dat con datos, las cuales deben generar el
archivo, leer valores, modificarlos, etc.
Utilice los algoritmos de los ejercicios anteriores para implementar los m�todos.
*/

#include <iostream>
#include <cstdlib>
#include <fstream>
#include <time.h>

using namespace std;

struct tablas{
    int tab;
    double tab1;
};

void generar_grupo(tablas t[], int p);

void leer_valores(tablas tabl[], int n);

void modificar(tablas ta[], int m);

int main(int argc, char *argv[]) {
    tablas tabla[200];
    int tam=200;
    char opcion;

    generar_grupo(tabla, tam);

    int valor21=tabla[20].tab;

    opcion='1';

    do{
        switch(opcion){
            case '1': leer_valores(tabla, tam);
                    break;
            case '2': modificar(tabla, tam);
                    cout<<endl<<"Valor del entero de la linea 21 antes de ser modificado: "<<valor21<<endl<<endl;
                    break;
            case '3': leer_valores(tabla, tam);
                    for (int i=0;i<tam;i++){
                        cout<<i+1<<") "<<tabla[i].tab<<" "<<tabla[i].tab1<<endl;
                    }
                    break;
        }
        cout<<"Menu:"<<endl;
        cout<<"1- Leer archivo grupo.dat"<<endl;
        cout<<"2- Modificar el dato entera de la linea 21 del archivo grupo.dat"<<endl;
        cout<<"3- Mostar el contenido de grupo.dat"<<endl;
        cout<<"0- Salir"<<endl;
        cout<<"opcion:"<<ends;
        cin>>opcion;
    }while (opcion!='0');

    system("pause");
	return 0;
}

void generar_grupo(tablas t[], int p){
    srand(time(0));
    fstream archi("grupo.dat", ios::out|ios::binary);

    for (int i=0;i<p;i++){
        t[i].tab=rand();
        t[i].tab1=((rand()/(RAND_MAX*1.0))*10000);
        archi.write((char*)&t[i], sizeof(tablas));
    }

    archi.close();
}

void leer_valores(tablas tabl[], int n){
    fstream archi("grupo.dat", ios::in|ios::binary);
    for (int i=0;i<n;i++){
        archi.read((char*)&tabl[i], sizeof(tablas));
    }
    archi.close();
}

void modificar(tablas ta[],int m){
    for (int i=0;i<m;i++){
        if (i==20){
            ta[i].tab=95;
            break;
        }
    }
    fstream archi("grupo.dat", ios::out|ios::binary);
    for (int i=0;i<m;i++){
        archi.write((char*)&ta[i], sizeof(tablas));
    }
    archi.close();
}
