#include <iostream>
#include <cstdlib>
#include <fstream>
#include <time.h>

using namespace std;

int main(int argc, char *argv[]) {
    srand(time(0));
    fstream archi("grupo.dat", ios::out|ios::binary);

    int tabla;
    double tabla1;

    for (int i=0;i<200;i++){
        tabla=rand();
        tabla1=((rand()/(RAND_MAX*1.0))*10000);
        archi.write((char*)&tabla, sizeof(int));
        archi.write((char*)&tabla1, sizeof(double));
        cout<<i+1<<") "<<tabla<<" "<<tabla1<<endl;
    }

    archi.close();
    system("pause");
	return 0;
}

