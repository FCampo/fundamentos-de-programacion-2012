#include <iostream>
#include <cstdlib>
#include <fstream>
#include <time.h>

using namespace std;

int main(int argc, char *argv[]) {
    srand(time(0));
    fstream archi1("grupo.dat", ios::in|ios::out|ios::binary);

    int tabla[200];
    double tabla1[200];

    cout<<"  ENTERO "<<" FLOTANTE  "<<endl;
    for (int i=0;i<200;i++){
        archi1.read((char*)&tabla[i], sizeof(int));
        archi1.read((char*)&tabla1[i], sizeof(double));
        if (i==20){
            cout<<i+1<<") "<<tabla[i]<<endl;
            tabla[i]=95;
        }
    }
    archi1.seekg(0, ios::beg);
    for (int i=0;i<200;i++){
        archi1.write((char*)&tabla[i], sizeof(int));
        archi1.write((char*)&tabla1[i], sizeof(double));
        cout<<i+1<<") "<<tabla[i]<<" "<<tabla1[i]<<endl;
    }


    archi1.close();
    system("pause");
	return 0;
}
