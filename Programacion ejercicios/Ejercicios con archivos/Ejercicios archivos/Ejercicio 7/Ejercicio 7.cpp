/*
Ejercicio 7
Dise�e una funci�n para generar un archivo binario que contenga datos de tipo struct correspondientes
a un grupo de personas. Los miembros del struct son apellido, nombres, dn, mn, an. Donde dn, mn,
an corresponden al d�a, mes y a�o de nacimiento de cada persona. Proponga otras para a) almacenar
los datos de cada persona, b) Determinar la edad de una persona, c) promediar las edades de todas
las personas.
*/

#include <iostream>
#include <cstdlib>
#include <fstream>
#include <cstring>
#include <ctime>

using namespace std;

struct personas{
    char apellido[50];
    char nombres[50];
    int dn;//Dia de nacimeinto
    int mn;//Mes de nacimineto
    int an;//A�o de nacimiento
};

void Generar_binario(personas pers[], int tam); //Genera el archivo binario

void Almacenar_personas(personas pers[], int tam); //a) almacenar los datos de cada persona

int Determinar_edad(personas pers[], int opc, int dia, int mes, int ano); //b) Determinar la edad de una persona

float Promedio_edad(personas pers[], int tam, int dia, int mes, int ano);//c) promediar las edades de todas las personas

int main(int argc, char *argv[]) {

    time_t tSac = time(NULL);       // instante actual
    struct tm* tmP = localtime(&tSac);
    int opcion;
    int edad;
    float prom;
    int tamano=3;
    int da=((int)tmP->tm_mday);
    int ma=((int)tmP->tm_mon+1);
    int aa=((int)tmP->tm_year+1900);
    personas persona[tamano];

    cout<<da<<" "<<ma<<" "<<aa<<endl<<endl;

    for (int i=0;i<tamano;i++){
        cout<<"Ingrese apellido:"<<ends;
        gets(persona[i].apellido);
        cout<<"Ingrese nombre:"<<ends;
        gets(persona[i].nombres);
        cout<<"Ingrese dia de nacimiento:"<<ends;
        cin>>persona[i].dn;
        cout<<"Ingrese mes de nacimineto:"<<ends;
        cin>>persona[i].mn;
        cout<<"Ingrese a�o de nacimiento:"<<ends;
        cin>>persona[i].an;
        cin.ignore();
    }

    //Genera el archivo binario
    Generar_binario(persona, tamano);
    //a) almacenar los datos de cada persona
    Almacenar_personas(persona, tamano);

    for (int i=0;i<tamano;i++){
        cout<<endl<<i+1<<") Apellido y nombre: "<<persona[i].apellido<<" "<<persona[i].nombres<<endl;
        cout<<"Dia, mes y a�o de nacimiento: "<<persona[i].dn<<" "<<persona[i].mn<<" "<<persona[i].an<<endl;
    }

    //b) Determinar la edad de una persona
    cout<<"La edad de que persona desea saber? (el numero de la persona lo puede ver en la lista de arriba)"<<endl;
    cin>>opcion;
    opcion=opcion-1;
    edad=Determinar_edad(persona, opcion, da, ma, aa);
    cout<<"Edad: "<<edad<<endl;

    //c) promediar las edades de todas las personas
    prom=Promedio_edad(persona, tamano, da, ma, aa);
    cout<<"Promedio de edades: "<<prom<<endl;

    system("pause");
    return 0;
}

//Genera el archivo binario
void Generar_binario(personas pers[], int tam){
    fstream archi("Personas.dat", ios::out|ios::binary);
    for (int i=0;i<tam;i++)
        archi.write((char*)&pers[i], sizeof(personas));
    archi.close();
}

//a) almacenar los datos de cada persona
void Almacenar_personas(personas pers[], int tam){
    fstream archi("Personas.dat", ios::out|ios::binary);
    for (int i=0;i<tam;i++)
        archi.read((char*)&pers[i], sizeof(personas));
    archi.close();
}

//b) Determinar la edad de una persona
int Determinar_edad(personas pers[], int opc, int dia, int mes, int ano){
    int edadd;
    if (pers[opc].dn<dia){
        if (pers[opc].mn>=mes){
            edadd=((ano-(pers[opc].an))-1);
        }
        else if (pers[opc].mn<mes){
                edadd=(ano-(pers[opc].an));
        }
    }
    else
        if(pers[opc].dn>=dia){
            if (pers[opc].mn>mes){
                edadd=((ano-(pers[opc].an))-1);
            }
            else if (pers[opc].mn<=mes){
                edadd=(ano-(pers[opc].an));
            }
        }
    return edadd;
}

//c) promediar las edades de todas las personas
float Promedio_edad(personas pers[], int tam, int dia, int mes, int ano){
    float Promedio=0.0;
    int acum=0;
    int edades;
    for (int i=0;i<tam;i++){
        if (pers[i].dn<dia){
                if (pers[i].mn>=mes){
                    edades=((ano-(pers[i].an))-1);
                }
            else if (pers[i].mn<mes){
                    edades=(ano-(pers[i].an));
            }
        }
        else{
            if(pers[i].dn>=dia){
                if (pers[i].mn>mes){
                    edades=((ano-(pers[i].an))-1);
                }
                else if (pers[i].mn<=mes){
                    edades=(ano-(pers[i].an));
                }
            }
        }
        acum+=edades;
    }
    Promedio=(acum/(tam*1.0));
    return Promedio;
}
