#include <iostream>
#include <cstdlib>
#include <fstream>
#include <cstring>

using namespace std;

int main(int argc, char *argv[]) {

    ifstream leer ("Lista.txt");
    ofstream esc ("Lista con email y condicion.txt");
    char cadena[50], ap[50], *nom, espacio=' ';
    int n1, n2, n3;
    float prom;

    while (leer.getline(cadena, 50)){
        leer>>n1>>n2>>n3;
        cout<<cadena<<endl;
        esc<<cadena<<endl;

        nom=strchr(cadena, espacio);
        strlwr(nom);
        for (int i=0;i<strlen(nom);i++){
            nom[i]=nom[i+1];
        }
        strncpy(ap, cadena, 1);
        ap[1]='\0';
        strlwr(ap);
        strcat(ap, nom);
        strcat(ap, "@bioingenieria.edu.ar");
        cout<<ap<<endl;
        esc<<ap<<endl;

        cout<<n1<<" "<<n2<<" "<<n3<<endl;
        esc<<n1<<" "<<n2<<" "<<n3<<endl;
        prom=((n1+n2+n3)/3);

        if (prom>=80){
            cout<<"Promociona"<<endl;
            esc<<"Promociona"<<endl;
        }
        else if (prom>60){
            cout<<"Regulariza"<<endl;
            esc<<"Regulariza"<<endl;
        }
        else{
            cout<<"Libre"<<endl;
            esc<<"Libre"<<endl;
            }
        esc<<endl;
        cout<<endl;
        leer.ignore();
    }

    leer.close();
    esc.close();
    system("pause");
	return 0;
}

