#include <iostream>
using namespace std;

int main(int argc, char *argv[]) {
	
    int a=90;
    int *p=&a;
    int b=(*p)++;
    int *q=p+2;
    cout<<p<<" "<<*p<<endl;
    cout<<q<<" "<<*q<<endl;
    cout<<a<<" "<<b<<endl;
    p++;
    b=*(q--)-1;
    a=(*p++)+1;
    cout<<a<<" "<<b<<endl;
	
	system ("pause");
    return 0;
}

