#include <iostream>
#include <cstring>
using namespace std;

int main(int argc, char *argv[]) {
    int a=45; // la variable a se ha almacenado en la direcci�n 0x22ff3c 
    int b[]={33,95,12}; // la variable b se ha almacenado en la direcci�n 0x22ff38 
    char c[20] ={"Hola que tal?"}; // la variable c se ha almacenado en la direcci�n 0x22ff1c
    int *p1=&a; // la variable p1 se ha almacenado en la direcci�n 0x22ff18
    char *p2; // la variable p2 se ha almacenado en la direcci�n 0x22ff14
    
    cout<<"Resultados"<<endl;
    cout<<"a) "<<*p1<<endl; 
    p2=c;
    p2++;
    cout<<"b) "<<*p2<<endl; 
    p1=&b[1]; 
    cout<<"c) "<<*p1+1<<endl;
    p2=strstr(c,"que");
    cout<<"d) "<<*p2<<endl; 
    cout<<"e) "<<p1+1<<endl; 
    cout<<"f) "<<p2<<endl; 
    p1=b;
    cout<<"g) "<<p1<<endl; 

    system("pause");
	return 0;
}
