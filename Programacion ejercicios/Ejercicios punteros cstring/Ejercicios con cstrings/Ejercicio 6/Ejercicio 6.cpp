/**
*Ejercicio 6
*Escriba un programa que permita ingresar una lista de apellidos y nombres de N
*profesores de la FI-UNER (apellido y nombre se asignan a una sola variable). El programa
*debe mostrar las direcciones de correo electrónicos (e-mails) de ellos. El dominio
*asignado a la Facultad para el e-mail es: fi.uner.edu.ar, y el nombre de usuario se forma
*con la inicial del nombre y el apellido.
*/

#include <iostream>
#include <cstdlib>
#include <stdio.h>
#include <cstring>

using namespace std;

int main(int argc, char *argv[]) {
    int tamanio;
    cout<<"Ingrese la cantidad de profesores: "<<endl;
    cin>>tamanio;
    char **profesores;
    profesores=new char* [tamanio];
    for (int i=0;i<tamanio;i++){
        profesores[i]=new char [100];
    }
    char apellido[50];
    char *nombre;

    cin.ignore();
    for (int i=0;i<tamanio;i++){
        cout<<endl<<"Ingresar apellido y nombre: "<<endl;
        gets(profesores[i]);
        for (int e=0;e<strlen(profesores[i]);e++){
            apellido[e]=profesores[i][e];
            if (profesores[i][e]==' '){
                apellido[e]='\0';
                break;
            }
        }
        nombre=strchr(profesores[i], ' ');
        nombre[0]=nombre[1];
        nombre[1]='\0';
        strcat(nombre, apellido);
        strcat(nombre, "@fi.uner.edu.ar");
        cout<<endl<<"Email:"<<endl;
        strlwr(nombre);
        puts(nombre);
    }
    cout<<endl;
    for (int i=0;i<tamanio;i++){
        delete[] profesores[i];
    }
    delete[] profesores;
    system("pause");
    return 0;
}
