/**
*Ejercicio 5
*Escriba un programa que permita ingresar una lista de apellidos y nombres de N personas
*(apellido y nombre se asignan a una sola variable). Utilice para cada persona una variable
*de tipo cstring. El programa debe informar un listado con los N apellidos y luego un listado
*con los N nombres.
*Nota: Considere apellidos formados por una o m�s palabras hasta encontrar una coma
*(�,�). Considere la posibilidad de que una persona posea m�s de un nombre. Cree los
*cstrings en forma din�mica.
*/

#include <iostream>
#include <cstdlib>
#include <cstring>
#include <stdio.h>

using namespace std;

int main(int argc, char *argv[]) {
    int tamanio;
    cout<<"Ingrese la cantidad de personas: "<<endl;
    cin>>tamanio;
    char **persona;
    persona=new char* [tamanio];
    for (int i=0;i<tamanio;i++){
        persona[i]=new char [100];
    }
    char apellido[50];
    char *nombre;

    cin.ignore();
    for(int i=0;i<tamanio;i++){
        cout<<"Ingrese apellidos y nombres separados por una coma (','):"<<endl;
        gets(persona[i]);
    }

    for (int i=0;i<tamanio;i++){
        for (int e=0;e<strlen(persona[i]);e++){
            apellido[e]=persona[i][e];
            if (persona[i][e]==','){
                apellido[e]='\0';
                break;
            }
        }
        puts(apellido);
    }

    for (int i=0;i<tamanio;i++){
        nombre=strchr(persona[i], ',');
        for (int e=0;e<strlen(nombre);e++){
            nombre[e]=nombre[e+2];
        }
        puts(nombre);
    }

    for (int i=0;i<tamanio;i++){
        delete[] persona[i];
    }
    delete[] persona;
    system("pause");
    return 0;
}
