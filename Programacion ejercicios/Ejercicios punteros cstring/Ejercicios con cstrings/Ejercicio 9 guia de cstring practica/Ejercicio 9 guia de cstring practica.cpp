/**
*
*Ejercicio 9
*
*Calcule el promedio de N datos num�ricos (correspondientes a calificaciones de un curso
*de N estudiantes. Ingrese los datos num�ricos empleando cstrings y valide la entrada,
*solicitando de nuevo el dato si este fuera no v�lido (si por error no se ha ingresado un
*n�mero).
*
*/

#include <iostream>
#include <cstdlib>
#include <stdio.h>

using namespace std;

int main(int argc, char *argv[]) {

    int Promedio=0;
    int n_est;//N�mero de estudiantes;
    int n_not;//N�mero de calificaciones;
    cout<<"Ingrese la cantidad de estudiantes:"<<ends;
    cin>>n_est;
    cout<<"Ingrese la cantidad de calificaciones:"<<ends;
    cin>>n_not;
    char calif[4];
    int calif2;
    int **Numeros;
    Numeros=new int *[n_est];
    for(int i=0;i<n_est;i++){
        Numeros[i]=new int [n_not];
    }

    cin.ignore();
    for (int i=0;i<n_est;i++){
        for (int e=0;e<n_not;e++){
            cout<<"Ingrese la calificacion "<<e+1<<" del alumno "<<i+1<<endl;
            gets(calif);
            calif2=atoi(calif);
            while((calif2<1)||(calif2>100)){
                cout<<"Ingrese la calificacion "<<e+1<<" del alumno "<<i+1<<endl;
                gets(calif);
                calif2=atoi(calif);
            }
            Numeros[i][e]=calif2;
        }
    }

    for (int i=0;i<n_est;i++){
        cout<<"Alumno:"<<i+1<<endl;
        for (int e=0;e<n_not;e++){
            cout<<"Nota"<<e+1<<": "<<Numeros[i][e]<<endl;
        }
    }

    for (int i=0;i<n_est;i++){
        for (int e=0;e<n_not;e++){
            Promedio+=Numeros[i][e];
        }
    }
    Promedio=Promedio/(n_est*n_not);

    cout<<"Promedio: "<<Promedio<<endl;

    for (int i=0;i<n_est;i++){
        delete[] Numeros[i];
    }
    delete[] Numeros;
    system("pause");
    return 0;
}
