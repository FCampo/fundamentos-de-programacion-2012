/**
*Ejercicio 9
*Calcule el promedio de N datos num�ricos (correspondientes a calificaciones de un curso
*de N estudiantes. Ingrese los datos num�ricos empleando cstrings y valide la entrada,
*solicitando de nuevo el dato si este fuera no v�lido (si por error no se ha ingresado un
*n�mero).
*/

#include <iostream>
#include <cstdlib>
#include <stdio.h>

using namespace std;

int main(int argc, char *argv[]) {
    int cant_notas;
    int cant_alumnos;
    cout<<"Ingrese la cantidad de notas:"<<endl;
    cin>>cant_notas;
    cout<<"Ingrese la cantidad de alumnos:"<<endl;
    cin>>cant_alumnos;
    char nota[3];
    int **notas;
    notas=new int* [cant_alumnos];
    for (int i=0;i<cant_alumnos;i++){
        notas[i]=new int [cant_notas];
    }

    cin.ignore();
    for (int i=0;i<cant_alumnos;i++){
        for (int e=0;e<cant_notas;e++){
            cout<<"Ingrese la nota "<<e+1<<" del alumno "<<i+1<<":"<<endl;
            gets(nota);
            notas[i][e]=atoi(nota);
            cin.ignore();
            while ((nota<'0')||(nota>'100')){
                cout<<"Ingrese la nota "<<e+1<<" del alumno "<<i+1<<":"<<endl;
                gets(nota);
                notas[i][e]=atoi(nota);
                cin.ignore();
            }
        }
    }

    for(int i=0;i<cant_alumnos;i++){
        for(int e=0;e<cant_notas;e++){
            cout<<notas[i][e]<<endl;
        }
    }

    for (int i=0;i<cant_alumnos;i++){
        delete[] notas[i];
    }
    delete[] notas;
    system("pause");
    return 0;
}
