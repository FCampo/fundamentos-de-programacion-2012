/**
*Ejercicio 4
*Escriba un programa que permita ingresar una lista de apellidos y nombres de 10
*personas (apellido y nombre se asignan a una sola variable). Utilice para cada persona
*una variable de tipo cstring. El programa debe informar un listado con los 10 apellidos y
*luego un listado con los 10 nombres.
*Nota: Considere apellidos formados por una �nica palabra. La presencia del primer
*espacio en blanco indica el fin del apellido.
*/

#include <iostream>
#include <cstdlib>
#include <stdio.h>
#include <cstring>

using namespace std;

int main(int argc, char *argv[]) {
    char persona[10][100];
    char apellido[50];
    char *nombre;

    for (int i=0;i<10;i++){
        cout<<"Ingrese apellido y nombre:"<<endl;
        gets(persona[i]);
    }
    for (int e=0;e<10;e++){
        for (int i=0;i<strlen(persona[e]);i++){
            apellido[i]=persona[e][i];
            if (persona[e][i]==' '){
                apellido[i]='\0';
                puts(apellido);
                break;
            }
        }
    }

    for (int i=0;i<10;i++){
        nombre=strchr(persona[i], ' ');
        for (int e=0;e<strlen(nombre);e++){
            nombre[e]=nombre[e+1];
        }
        puts(nombre);
    }

    system("pause");
    return 0;
}
