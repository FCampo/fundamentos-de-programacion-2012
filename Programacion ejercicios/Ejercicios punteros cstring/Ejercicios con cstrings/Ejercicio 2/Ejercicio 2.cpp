/**
*Ejercicio 2
*Escriba un programa que permita al usuario leer interactivamente un dato tipo cstring y
*exhiba un men� con las opciones:
*1. Pasar a May�sculas
*2. Pasar a Min�sculas
*3. Solo la inicial con may�sculas.
*El programa debe resolver la opci�n seleccionada por el usuario.
*/
#include <iostream>
#include <cstdlib>
#include <stdio.h>
#include <ctype.h>
#include <cstring>

using namespace std;

int main(int argc, char *argv[]) {
    int opcion;
    char palabra[50];

    cout<<"Ingresar la palabra: "<<ends;
    cin.getline(palabra, 50);
    cout<<"Menu:"<<endl;
    cout<<"1. Pasar a May�sculas."<<endl;
    cout<<"2. Pasar a Min�sculas."<<endl;
    cout<<"3. Solo la inicial con may�sculas."<<endl;
    cout<<"Ingresar una opcion: "<<ends;
    cin>>opcion;

    switch (opcion){
    case 1:
        strupr(palabra);
        puts(palabra);
        break;
    case 2:
        strlwr(palabra);
        puts(palabra);
        break;
    case 3:
        palabra[0]=toupper(palabra[0]);
        cout<<palabra<<endl;
        break;
    }

    system("pause");
    return 0;
}
