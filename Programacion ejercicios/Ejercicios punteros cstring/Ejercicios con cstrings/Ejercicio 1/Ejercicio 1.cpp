/**
*Ejercicio 1
*Realice un programa para leer un arreglo lineal de N elementos conteniendo palabras de
*hasta 12 caracteres. Luego el programa deber� informar separadamente:
*a) El primero de la lista de acuerdo a un orden alfab�tico.
*b) Los elementos del arreglo que ocupan las posiciones pares.
*c) Las palabras que comienzan con la s�laba �mar�.
*/

#include <iostream>
#include <cstdlib>
#include <cstring>
#include <stdio.h>

using namespace std;

//a) El primero de la lista de acuerdo a un orden alfab�tico.
int Primero_Lista(char **arreg, int tam);

int main(int argc, char *argv[]) {
    int tamanio;
    cout<<"Ingrese la cantidad de palabras:"<<endl;
    cin>>tamanio;
    //Creando arreglo dinamico.
    char **arreglo;
    arreglo=new char* [tamanio];
    for (int i=0;i<tamanio;i++){
        arreglo[i]=new char [12];
    }

    cin.ignore();
    for (int i=0;i<tamanio;i++){
        cout<<"Ingrese la "<<i+1<<" palabra:"<<endl;
        gets(arreglo[i]);
    }

    for (int i=0;i<tamanio;i++){
        cout<<endl<<i+1<<" palabra"<<endl;
        puts(arreglo[i]);
    }

    //a) El primero de la lista de acuerdo a un orden alfab�tico.
    int prim=Primero_Lista(arreglo, tamanio);
    cout<<endl<<"El primero de la lista de acuerdo a un orden alfabetico es: "<<arreglo[prim]<<endl<<endl;

    //b) Los elementos del arreglo que ocupan las posiciones pares.
    for (int i=0;i<tamanio;i++){
        if (((i+1)%2)==0){
            cout<<"La palabra que ocupa el elemento par "<<i+1<<" es: "<<arreglo[i]<<endl;
        }
    }
    //c) Las palabras que comienzan con la s�laba �mar�.
    char mar[]="mar";
    int cont=1;
    cout<<endl<<"Lista de palabras que comienzan con la silaba mar: "<<endl;
    for (int i=0;i<tamanio;i++){
        if ((strncmp(arreglo[i], mar, 3))==0){
            cout<<cont<<") "<<arreglo[i]<<endl;
            cont++;
        }
    }
    cout<<endl;

    //Eliminando arreglo dinamico.
    for (int i=0;i<tamanio;i++){
        delete[] arreglo[i];
    }
    delete[] arreglo;
    system("pause");
    return 0;
}

int Primero_Lista(char **arreg, int tam){
    int primero=0;

    for (int i=0;i<tam;i++){
        if ((strcmp(arreg[primero], arreg[i]))>0){
            primero=i;
        }
    }

    return primero;
}
