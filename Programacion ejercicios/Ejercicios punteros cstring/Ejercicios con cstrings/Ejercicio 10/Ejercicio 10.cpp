/**
*Ejercicio 10
*La llamada a la funci�n subcadena(s,i,n) se hace con tres par�metros: un cstring s y 2
*n�meros enteros i y n. La funci�n devuelve el substring que se inicia en el car�cter i del
*cstring s y tiene una longitud de n caracteres. a) Escriba su c�digo. b) proponga un
*programa C++ que la utilice.
*/
#include <iostream>
#include <cstdlib>
#include <stdio.h>

using namespace std;

char *subcadena(char* s, int i, int n);

int main(int argc, char *argv[]) {
    char cadena[150];
    char *cade;
    int desde, hasta;

    cout<<"Ingrese la cadena: "<<endl;
    gets(cadena);
    cout<<"Ingrese desde donde quiere copiar: "<<endl;
    cin>>desde;
    cout<<"Ingrese hasta donde quiere copiar: "<<endl;
    cin>>hasta;

    cade=subcadena(cadena, desde, hasta);

    puts(cade);

    system("pause");
    return 0;
}
char *subcadena(char* s, int i, int n){
    char *sub;
    int cont=0;
    for (int e=i;e<n;e++){
        sub[cont]=s[e];
        cont++;
    }
    sub[cont]='\0';
    return sub;
}
