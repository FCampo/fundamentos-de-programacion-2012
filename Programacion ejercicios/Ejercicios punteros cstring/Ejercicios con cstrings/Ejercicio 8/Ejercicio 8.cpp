/**
*Ejercicio 8
*a) Escriba una funci�n entera llamada quita_ceros( ) que reciba un arreglo de enteros
*como par�metro y su longitud y elimine los ceros del mismo. Debe devolver la cantidad de
*elementos eliminada.
*b) Escriba una sobrecarga de la funci�n anterior que reciba solo un cstring de par�metro y
*elimine sus espacios en blanco.
*/

#include <iostream>
#include <cstdlib>
#include <time.h>
#include <cstring>
#include <stdio.h>

using namespace std;

///a) Elimine los ceros del arreglo de enteros, y devolver la cantidad de elementos eliminados.
int quita_ceros(int arre[], int tam);
///b) sobrecarga de la funci�n anterior que elimine sus espacios en blanco.
void quita_ceros(char *cstring);

int main(int argc, char *argv[]) {
    srand(time(0));
    int tamanio=10;
    int arreglo[tamanio];
    int cantidad_eliminada;
    char cadena[100];

    for (int i=0;i<tamanio;i++){
        arreglo[i]=rand()%5;
    }
    for (int i=0;i<tamanio;i++){
        cout<<arreglo[i]<<ends;
    }

    cantidad_eliminada=quita_ceros(arreglo, tamanio);
    cout<<endl<<"Cantidad de ceros eliminados: "<<cantidad_eliminada<<endl;
    for (int i=0;i<(tamanio-cantidad_eliminada);i++){
        cout<<arreglo[i]<<ends;
    }

    cout<<endl<<"Ingrese la cadena que desea eliminar los espacios:"<<endl;
    gets(cadena);
    quita_ceros(cadena);
    cout<<"Cadena sin espacios:"<<endl;
    puts(cadena);

    system("pause");
    return 0;
}

///a) Elimine los ceros del arreglo de enteros, y devolver la cantidad de elementos eliminados.
int quita_ceros(int arre[], int tam){
    int cont=0;
    int i=0;
    while(i<tam){
        if (arre[i]==0){
            for (int e=i;e<tam-cont;e++){
                arre[e]=arre[e+1];
            }
            cont++;
        }
        else i++;
    }
    return cont;
}

///b) sobrecarga de la funci�n anterior que elimine sus espacios en blanco.
void quita_ceros(char *cstring){
    int i=0;
    int cont=0;
    int tam=strlen(cstring);
    while(i<tam){
        if (cstring[i]==' '){
            for (int e=i;e<tam-cont;e++){
                cstring[e]=cstring[e+1];
            }
            cont++;
        }
        else i++;
    }
}
