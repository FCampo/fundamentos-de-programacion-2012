/**
*Ejercicio 7
*Se ingresan las direcciones de correo electrónicos de un grupo de usuarios de Facultades
*de la UNER. Se debe determinar el número de usuarios de las Facultades de Ing. y
*Ciencias Económicas (ce) y de Trabajo Social (ts).
*Las direcciones tienen la denominación de la facultad a continuación del símbolo @.
*Ejemplos: jperez@ce.uner.edu.ar (C. Económicas), agarcia@ts.uner.edu.ar (T. Social),
*jprodo@fi.uner.edu.ar (F. Ingeniería), etc.
*/
#include <iostream>
#include <cstdlib>
#include <cstring>
#include <stdio.h>

using namespace std;

int main(int argc, char *argv[]) {
    int cont_t=0, cont_f=0, cont_c=0;
    int tamanio;
    cout<<"Ingrese la cantidad de correos a ingresar: "<<endl;
    cin>>tamanio;

    char **correos;
    correos=new char* [tamanio];
    for (int i=0;i<tamanio;i++){
        correos[i]=new char [100];
    }
    char *correo;

    cin.ignore();
    for (int i=0;i<tamanio;i++){
        cout<<"Ingrese el correo: "<<endl;
        gets(correos[i]);
        correo=strchr(correos[i], '@');
        switch (correo[1]){
        case 't':cont_t++;
            break;
        case 'f':cont_f++;
            break;
        case 'c':cont_c++;
            break;
        }
    }
    cout<<"Cantidad de usuarios de la facultad de ingenieria: "<<cont_f<<endl;
    cout<<"Cantidad de usuarios de la facultad de ciencias economicas: "<<cont_c<<endl;
    cout<<"Cantidad de usuarios de la facultad de trabajo social: "<<cont_t<<endl;

    for (int i=0;i<tamanio;i++){
        delete[] correos[i];
    }
    delete[] correos;
    system("pause");
    return 0;
}
