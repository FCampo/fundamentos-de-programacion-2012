/**
*Ejercicio 3
*Escriba un programa a partir del cual un usuario pueda ingresar una frase cualquiera y
*determine a trav�s de funciones: a) La cantidad de vocales de la frase; b) La cantidad de
*consonantes; c) La cantidad de letras.
*/
#include <iostream>
#include <cstdlib>
#include <stdio.h>
#include <ctype.h>
#include <cstring>

using namespace std;

///a) La cantidad de vocales de la frase.
int Cant_Vocales(char *frase, int tam);
///b) La cantidad de consonantes.
int Cant_Conso(char *frase, int tam);
///c) La cantidad de letras.
int Cant_Letras(char *frase, int tam);

int main(int argc, char *argv[]) {
    int tamanio=180;
    char frases[tamanio];
    int vocales;

    cout<<"Ingresar la frase:"<<endl;
    gets(frases);

    int tamreal=strlen(frases); //cantidad de caracteres antes del caracter de fin de linea('\0')

    ///a) La cantidad de vocales de la frase.
    vocales=Cant_Vocales(frases, tamreal);
    cout<<"Cantidad de vocales: "<<vocales<<endl;

    ///b) La cantidad de consonantes.
    int consonantes=Cant_Conso(frases, tamreal);
    cout<<"Cantidad de consonantes: "<<consonantes<<endl;

    ///c) La cantidad de letras.
    int letras=Cant_Letras(frases, tamreal);
    cout<<"Cantidad de letras: "<<letras<<endl;

    system("pause");
    return 0;
}

///a) La cantidad de vocales de la frase.
int Cant_Vocales(char *frase, int tam){
    int voc=0;
    for (int i=0;i<tam;i++){
        if ((tolower(frase[i])=='a')||(tolower(frase[i])=='e')||(tolower(frase[i])=='i')||(tolower(frase[i])=='o')||(tolower(frase[i])=='u')){
            voc++;
        }
    }
    return voc;
}

///b) La cantidad de consonantes.
int Cant_Conso(char *frase, int tam){
    int conso=0;
    for (int i=0;i<tam;i++){
        if ((tolower(frase[i])>'a')&&(tolower(frase[i])<='z')&&(tolower(frase[i])!='a')&&(tolower(frase[i])!='e')&&(tolower(frase[i])!='i')&&(tolower(frase[i])!='o')&&(tolower(frase[i])!='u')){
            conso++;
        }
    }
    return conso;
}

///c) La cantidad de letras.
int Cant_Letras(char *frase, int tam){
    int let=0;
    for (int i=0;i<tam;i++){
        if ((tolower(frase[i])>='a')&&(tolower(frase[i])<='z')){
            let++;
        }
    }
    return let;
}
