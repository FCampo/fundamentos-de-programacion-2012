/**
*
*Ejercicio 10
*
*La llamada a la funci�n subcadena(s,i,n) se hace con tres par�metros: un cstring s y 2
*n�meros enteros i y n. La funci�n devuelve el substring que se inicia en el car�cter i del
*cstring s y tiene una longitud de n caracteres. a) Escriba su c�digo. b) proponga un
*programa C++ que la utilice.
*
*/

#include <iostream>
#include <cstdlib>
#include <stdio.h>

using namespace std;

char* subcadena(const char* s, int i, int n);

int main(int argc, char *argv[]) {

    char cadena[40]="Internet es la red de redes";
    int l=9, f=8;
    char* p;

    p=subcadena(cadena, l, f);

    puts(p);

    system("pause");
    return 0;
}

char* subcadena(const char* s, int i, int n){

    char* puntero;
    puntero=(char*)s;

    puntero=&puntero[i];
    puntero[n]='\0';

    return puntero;
}
