/**
*
* Ejercicio 1
*
* Un grupo de investigadores est� realizando un estudio con una droga experimental para controlar la diabetes.
* Para ello desea analizar los datos de cierto grupo de ratones que han utilizado la droga.
* Este grupo se divide en dos: los ratones sanos (grupo de control) y los ratones con diabetes inducida (grupo tratado).
* Cada rat�n se identifica con un c�digo num�rico de cinco cifras, los n�meros pares corresponden a ratones del grupo de control y
* los impares a ratones del grupo tratado. Sobre cada rat�n se realiza una prueba de tolerancia a la glucosa,
* para lo cual se toma una muestra de sangre y se mide el valor de glucosa en sangre en ayunas (valor basal) y
* luego de ingerir un alimento especial, se tomas tres valores de glucosa cada media hora.
* Escriba un algoritmo en diagrama de flujos o pseudoc�digo que:
*
* a) Lea para cada uno de los ratones que participaron en el estudio: su c�digo, el valor de glucosa basal (Cgb) y
* los tres valores de glucosa en sangre medidos luego de la ingesta, Cg1, Cg2 y Cg3 (en ese orden).
* La carga de datos finaliza con un valor de c�digo igual a cero.
*
* b) Determinar e informar el porcentaje de ratones tratados que tuvieron su m�xima concentraci�n de glucosa en sangre
* a la media hora (primera medici�n), el porcentaje que tuvo la m�xima concentraci�n a la hora (segunda medici�n) y
* el porcentaje que tuvo su m�ximo valor a la hora y media (�ltima medici�n).
*
* c) Informar el c�digo del rat�n que present� la menor concentraci�n de glucosa basal y a que grupo pertenece.
*
* d) Determinar e informar el promedio de glucosa final en sangre (Cg3) de cada grupo.
*
*/

#include <iostream>
#include <cstdlib>
#include <iomanip>

using namespace std;

int main(int argc, char *argv[]) {

    int Rat; //Grupos de ratones(par es grupo de control, impar es grupo de trabajo).
    float Cgb, Cg1, Cg2, Cg3; //Valor de glucosa en sangre (en ayunas, media hs despues, una hs despues, una hs y media despues).
    int gmax1=0, gmax2=0, gmax3=0;//Glucosa maxima cada media hora.
    float Menor_Cgb=100000; //Menor valor de glucosa basal.
    int Grupo, Cod_Rat;
    float Porc1=0, Porc2=0, Porc3=0;
    int cont=0, contg1=0, contg2=0;
    float Prom_G1, Prom_G2, acumcg31=0, acumcg32=0;

    ///PUNTO A:
    cout<<"Ingrese el codigo del raton (0 para finalizar el ingreso de datos): "<<ends;
    cin>>Rat;
    while (Rat!=0){
        cout<<"Ingrese el valor de glucosa basal: "<<ends;
        cin>>Cgb;
        //PARTE DEL PUNTO C:
        if (Cgb<Menor_Cgb){
            Menor_Cgb=Cgb;
            Cod_Rat=Rat;
        }
        //
        cout<<"Ingrese el valor de glucosa 1: "<<ends;
        cin>>Cg1;
        cout<<"Ingrese el valor de glucosa 2: "<<ends;
        cin>>Cg2;
        cout<<"Ingrese el valor de glucosa 3: "<<ends;
        cin>>Cg3;
        //PARTE DEL PUNTO B:
        if ((Cg1>=Cg2)&&(Cg1>=Cg3)){
            gmax1++;
        }
        else
        if((Cg2>=Cg1)&&(Cg2>=Cg3)){
            gmax2++;
        }
        else
        if((Cg3>=Cg1)&&(Cg3>=Cg2)){
            gmax3++;
        }
        cont++;
        //PARTE DEL PUNTO D:
        if((Rat%2)==0){
            contg1++;
            acumcg31+=Cg3;
        }
        else{
            contg2++;
            acumcg32+=Cg3;
        }
        //
        cout<<"Ingrese el codigo del raton (0 para finalizar el ingreso de datos): "<<ends;
        cin>>Rat;
    }
    cout<<endl;

    ///PUNTO B:
    Porc1=((gmax1*100.00)/cont);
    Porc2=((gmax2*100.00)/cont);
    Porc3=((gmax3*100.00)/cont);
    cout<<"Porcentaje de ratones con la maxima concentracion de glucosa\na la media hora: "<<Porc1<<endl;
    cout<<"a la hora: "<<Porc2<<endl;
    cout<<"a la hora y media: "<<Porc3<<endl<<endl;

    ///PUNTO C:
    if ((Cod_Rat%2)==0){
        cout<<"Codigo del raton con menor cantidad de glucosa basal: "<<Cod_Rat<<endl;
        cout<<"Cantidad de glucosa basal: "<<Menor_Cgb<<endl;
        cout<<"Pertenece al grupo de control"<<endl;
    }
    else {
        cout<<"Codigo del raton con menor cantidad de glucosa basal: "<<Cod_Rat<<endl;
        cout<<"Cantidad de glucosa basal: "<<Menor_Cgb<<endl;
        cout<<"Pertenece al grupo de trabajo"<<endl;
    }
    cout<<endl;

    ///PUNTO D:
    Prom_G1=acumcg31/contg1;
    Prom_G2=acumcg32/contg2;
    cout<<"Promedio de glucosa final en el grupo de control: "<<Prom_G1<<endl;
    cout<<"Promedio de glucosa final en el grupo de trabajo: "<<Prom_G2<<endl<<endl;

    system("pause");
    return 0;
}
