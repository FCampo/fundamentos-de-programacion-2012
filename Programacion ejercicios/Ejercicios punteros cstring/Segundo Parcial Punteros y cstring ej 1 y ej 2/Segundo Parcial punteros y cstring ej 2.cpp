#include <iostream>
using namespace std;

int main(int argc, char *argv[]) {
	int a=36; // la variable a se ha almacenado en la direcci�n 0x22ff3c 
    int b[9]={42,20,4}; // la variable b se ha almacenado a partir de la direcci�n 0x22fef0 
    char c[] ={"Programar en C++ es interesante"}; // c se ha almacenado a partir de 0x22ff14
    int *p1=&b[4]; // la variable p1 se ha almacenado en la direcci�n 0x22ff38
    char *p2;	// la variable p2 se ha almacenado en la direcci�n 0x22ff34
    cout<<"a) "<<p1<<endl; 
    p2=&c[3];
    cout<<"b) "<<p2<<endl;
    p2++;
    cout<<"c) "<<*p2<<endl; 
    p1=p1-4;
    cout<<"d) "<<*p1+4<<endl;
    p2=strchr(c,'r');
    cout<<"e) "<<p2<<endl; 
    p1=&a;
    cout<<"f) "<<p1+2<<endl; 
    cout<<"g) "<<&p2<<endl;
    system ("pause");
	return 0;
}

